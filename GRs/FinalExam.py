#!/usr/bin/env python3
"""
Final Exam, CS210, Fall 2017
"""
import csv
import json

import os
from pprint import pprint
__author__ = "Sarah Placke"  # Your name. Ex: John Doe
__section__ = "T6"  # Your section. Ex: M1
__version__ = "A"  # Test version. Ex: A
__date__ = "12 Dec 2017"  # Today's date. Ex: 25 Dec 2017


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    # problem5()
    # problem6()
    # problem7()
    problem8()


# ======================================================================
# Problem 1                                                ____ / 30 pts
# ======================================================================
def problem1():
    print_problem_name()

    # TODO 1b
    if nautical_miles_to_feet(1) != 0.0:
        print("Error: 1")
    if nautical_miles_to_feet(200) != 1215224.0:
        print("Error: 1")
    if nautical_miles_to_feet(6000) != 0.0:
        print("Error: 1")


# TODO 1a
def nautical_miles_to_feet(miles):
    feet = miles*6076.12
    statue_miles = feet/5280
    if statue_miles>=115 and statue_miles<=400:
        return float(feet)
    else:
        return float(0)


# ======================================================================
# Problem 2                                                ____ / 30 pts
# ======================================================================
def problem2():
    print_problem_name()

    # TODO 2
    list_of_words = []
    with open("../Data/alice_in_wonderland.txt") as data_file:
        data = data_file.read().splitlines()
    for line in data:
        words = line.split()
        longest = 0
        longest_word = " "
        for word in words:
            if len(word)>longest:
                longest = len(word)
                longest_word = word
        list_of_words += [longest_word]
    with open("../Data/alice_in_wonderland.txt") as data_file, open("alicewords.txt", "w") as alice_file:
        for item in list_of_words:
            alice_file.write(item+" ")


# ======================================================================
# Problem 3                                                ____ / 30 pts
# ======================================================================
def problem3():
    print_problem_name()

    # TODO 3b
    if find_min([3,67,45,23,18,4,89],3,5) != 4:
        print("Error: [3,67,45,23,18,4,89],3,5 ")
    if find_min([42,78,14,13,85,27],2,7) != 13:
        print("Error: [42,78,14,13,85,27],2,7 ")
    if find_min([22,58,12,10],-1,2)!= 12:
        print("Error: [22,58,12,10],-1,2 ")

# TODO 3a
def find_min(integer_list,first,last):
    """
        Calculates and returns the minimum value of a section of a list of values.

        :param list integer_list: The list of values for which the section is to be taken.
        :param int first: The index of the first value of the desired section (The start of the specific section)
        :param int last: The index of the last value of the desired section (inclusive) (The end of the specific section)
        :return: The minimum value of the specific section.
        :rtype: int
        """
    if first<0:
        first = 0
    if last > len(integer_list):
        last = len(integer_list)
    return sorted(integer_list[first:last+1])[0]





# ======================================================================
# Problem 4                                                ____ / 30 pts
# ======================================================================
def problem4():
    print_problem_name()

    # TODO 4c
    board1 = create_checkers(6)
    print("Checker Board (6)")
    print_checkers(board1)
    board2 = create_checkers(9)
    print("\nChecker Board (9)")
    print_checkers(board2)


# TODO 4a
def create_checkers(board_size):
    board = []
    counter = "no"
    column1 = []
    for rows in range(board_size):
        row = []
        for columns in range(board_size):
            if columns == 0 and rows != 0:
                if column1[columns-1] != "-":
                    counter = "no"
                else:
                    counter = "yes"
            if rows <= 2:
                if counter == "yes":
                    row += ["B"]
                    if len(row) != board_size:
                        counter = "no"
                else:
                    row += ["-"]
                    if len(row) != board_size:
                        counter = "yes"

            elif rows >= board_size - 3:
                if counter == "yes":
                    row += ["R"]
                    if len(row) != board_size:
                        counter = "no"

                else:
                    row += ["-"]
                    if len(row) != board_size:
                        counter = "yes"

            else:
                row += ["-"]
            column1 += [row[0]]
        board += [row]
    return board

# TODO 4b
def print_checkers(board):
    for row in board:
        for column in row:
            print(column + " ", end="")
        print(flush=True)


# ======================================================================
# Problem 5                                                ____ / 30 pts
# ======================================================================
def problem5():
    print_problem_name()

    # TODO 5b
    brick1 = Brick(30,10,80)
    brick2 = Brick(30,10,80)
    brick3 = Brick(10,20,30)
    if brick1.equals(brick2) != True:
        print("Error: brick1 DOES equal brick2")
    if brick1.equals(brick3) != False:
        print("Error: brick1 does NOT equal brick3")

# TODO 5a
class Brick():
    def __init__(self, length, width, height):
        self.length = length
        self.width = width
        self.height = height
        self.__volume = self.width*self.height*self.length
    def __str__(self):
        return "Your brick of length {}, width {}, and height {} has a volume of {}".format(self.length, self.width,
                                                                                            self.height, self.__volume)
    def equals(self,brick):
        if self.length == brick.length and self.width == brick.width and self.height == brick.height:
            return True
        else:
            return False

# ======================================================================
# Problem 6                                                ____ / 30 pts
# ======================================================================
def problem6():
    print_problem_name()

    # TODO 6b
    print("ATCG:\t {}".format(remove_duplicates("ATCG")))
    print("AAATCG:\t {}".format(remove_duplicates("AAATCG")))
    print("AATTTTCCGG:\t {}".format(remove_duplicates("AATTTTCCGG")))
    print("ATCGG:\t {}".format(remove_duplicates("ATCGG")))
    print("ATCGATTGAGCTCTAGG:\t {}".format(remove_duplicates("ATCGATTGAGCTCTAGG")))

# TODO 6a
def remove_duplicates(DNA):
    if len(DNA) <= 1:
        return str(DNA)
    elif len(DNA) ==2 and DNA[0] == DNA[1]:
        return str(DNA[0])
    elif DNA[0] == DNA[1]:
        while DNA[0] == DNA[1] and len(DNA)>1:
            DNA = DNA[1:]
        remove_duplicates(DNA)
    return str(DNA[0]) + str(remove_duplicates(DNA[1:]))




# ======================================================================
# Problem 7                                                ____ / 30 pts
# ======================================================================
def problem7():
    print_problem_name()
    perfect_squares = initiate(1,100)
    printing(1, 100, perfect_squares)
    other_perfect_squares = initiate(5,20)
    printing(5, 20, other_perfect_squares)


    # TODO 7
def initiate(start,end):
    perfect_squares = []
    for i in range(start,end):
        perfect_square_test = determining_factors(i)
        if perfect_square_test != None:
            perfect_squares += [perfect_square_test]
    return perfect_squares

def determining_factors(number):
    factors = []
    for i in range(1,number):
        if number%i == 0:
            factors += [i]
    if sum(factors) == number:
        return number
    else:
        return None
def printing(start,end,perfect_squares):
    print("Start:{}\nEnd:{}\nPerfect Numbers:".format(start, end))
    for number in perfect_squares:
        print(number)


# ======================================================================
# Problem 8                                                ____ / 40 pts
# ======================================================================
def problem8():
    print_problem_name()

    # TODO 8b
    dictionary = load_families("../Data/Names.txt")
    print(dictionary)

    # TODO 8c
    for family in dictionary:
        print(family)
        for person in dictionary[family]:
            print("\t",person["first_name"], person["middle_name"][0],".")



# TODO 8a
def load_families(filename):
    #
    with open(filename) as data_file:
        data_lines = data_file.read().splitlines()
    dictionary = {}
    mini_dictionary = {}

    for line in data_lines:
        last = (line.split()[2])
        first = line.split()[0]
        middle = line.split()[1]
        mini_dictionary["first_name"] = first
        mini_dictionary["middle_name"] = middle
        mini_dictionary["last_name"] = last
        dictionary[last] = [mini_dictionary]




    return dictionary



# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name]) or "No docstring"
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# d15606d9770cfd538b6d7c9c66cd23faa4bcb65a84818606563688257a8582d3 pfnenucynpxr
