#!/usr/bin/env python3
"""
GR2, CS210, Fall 2017
"""
import os
import random
import string
import easygui

# Fill in the metadata
__author__ = "Sarah Placke"  # Your name. Ex: John Doe
__section__ = "T6-7A"  # Your section. Ex: M1
__version__ = "B"  # Test version. Ex: A
__date__ = "27 Oct 2017"  # Today's date. Ex: 25 Dec 2017

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # Use an easygui.fileopenbox to get a filename from the user.
    filename = easygui.fileopenbox(default="../Data/*.txt")

    # Read the entire contents of a file into a single string.
    with open(filename, "r") as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open(filename, "r") as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open(filename, "r") as data_file:
        data_lines = data_file.read().splitlines()

    # Split a filename into its base and extension; build a new filename.
    base, ext = os.path.splitext(filename)
    output_filename = "{}_Output{}".format(base, ext)

    # Write a string to a file.
    with open(output_filename, "w") as output_file:
        output_file.write( "This is the output.\n" )
"""

import random
# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    # problem1()
    # problem2()
    # problem3()
    # problem4()
    # problem5()
    problem6()


# ======================================================================
# Problem 1                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem1():
    """Problem 1 from the GR."""
    print_problem_name()

    # TODO 1b
    ticket = generate_lotto(38)
    winner = generate_lotto(38)
    counter = 0
    for item in ticket:
        if item in winner:
            counter += 1
    print("Ticket: {} \nWinner: {}\nCount: {}".format(ticket,winner,counter))

# TODO 1a
def generate_lotto(upper_value):
    numbers = []
    while len(numbers)<6:
        number = random.randint(1,upper_value)
        if number not in numbers:
            numbers+= [number]
    return numbers

# ======================================================================
# Problem 2                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem2():
    """Problem 2 from the GR."""
    print_problem_name()

    board1 = [list("aa.bocc"),
              list("aa.bbcc"),
              list("qq.abb."),
              list("qqcaabb"),
              list("occaabb"),
              list("ccaacoo")]

    board2 = [list("q.c.oo.qb."),
              list("baa.boccbb"),
              list("baa.bbccba"),
              list("aqq.abb.ab"),
              list("aqqcaabbcq"),
              list("c.ccaabbcc"),
              list("cccaacooqc")]

    # TODO 2b
    print("Board 1, a:", count_pattern(board1, "a"))
    print("Board 1, b:", count_pattern(board1, "b"))
    print("Board 1, c:", count_pattern(board1, "c"))

    print("Board 2, a:", count_pattern(board2, "a"))
    print("Board 2, b:", count_pattern(board2, "b"))
    print("Board 2, c:", count_pattern(board2, "c"))

# TODO 2a
def count_pattern(nest, character):
    counter = 0
    for i in range(len(nest)):
        for j in range(1,len(nest[i])):
            if nest[i][j] == nest[i][j-1] == character:
                if i != (len(nest)-1) and nest[i+1][j] == character:
                    counter += 1
    return counter


# ======================================================================
# Problem 3                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem3():
    """Problem 3 from the GR."""
    print_problem_name()

    # TODO 3b
    filename = easygui.fileopenbox(default="../Data/*.txt")
    new_filename = "{}_madlib".format(filename.strip(".txt"))
    adjectives = ["tired","hot","sleepy","pink"]
    convert_to_madlib(filename,new_filename,adjectives)

# TODO 3a
def convert_to_madlib(filename,new_filename,words):
    with open(filename) as madlib:
        mad = madlib.readlines()
    file = open("../Data/newfile_madlib.txt", "w")
    message = ''
    firstly = 0
    for line in mad:
        newline = line.split()
        for word in newline:
            if word.lower() in words:
                message += ' _adjective_'
            elif word[-3:] == 'ing':
                message += '_verb_ing_'
            else:
                if word[len(word)-1]==".":
                    message += '\n'
                    message += word
                elif firstly == 0:
                    message += word
                else:
                    message += ' '+ word
                firstly = 1
    file.write(message)
# ======================================================================
# Problem 4                                                 ____ / 5 pts
# Grades: All or nothing
# ======================================================================
def problem4():
    """Problem 4 from the GR."""
    print_problem_name()

    # TODO 4
    answer = "G E A B F C D"  # Put your answer here, eg, "L M N O P"
    print("Sequence:", answer)


# ======================================================================
# Problem 5                                                ____ / 20 pts
# Grades: A: 20, B: 16, C: 14, D: 12, F: 10
# ======================================================================
def problem5():
    """Problem 5 from the GR."""
    print_problem_name()

    # TODO 5b
    print("" == recursive_extract_consonants(""))
    print("bnns" == recursive_extract_consonants("bananas"))
    print("nwyjs" == recursive_extract_consonants("no wayjose"))

# TODO 5a
def recursive_extract_consonants(string):
    vowels = "a","e","i","o","u","A","E","I","O","U"
    if len(string)==1 or len(string) == 0:
        if string not in vowels:
            answer = string
            return answer
        else:
            answer = ''
            return answer
    elif string[0] not in vowels:
        answer = string[0] + recursive_extract_consonants(string[1:])
    elif string[0] in vowels:
        answer = recursive_extract_consonants(string[1:])
    return answer



# ======================================================================
# Problem 6                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem6():
    """Problem 6 from the GR."""
    print_problem_name()

    # TODO 6c
    updates = load_updates("../Data/update_inventory.txt")
    inventory = {"apples": 900, "bananas": 800}
    print("Before:",inventory)
    update_inventory(inventory,updates)
    print("After:",inventory)

# TODO 6b
def update_inventory(dict,updates):
    for item in updates:
        if item not in dict:
            dict[item] = int(item[1])
        else:
            dict[item] = int(dict[item])+ int(item[1])



# TODO 6a
def load_updates(filename):
    with open(filename) as new:
        lines = new.readlines()
    list1 = []
    list2 = []
    new_list =[]
    for item in lines:
        word = ''
        new_integer = ''
        transition = 0
        for i in range(len(item)):
            print (item[i])
            if item[i].isnumeric() and transition == 0:
                list1 += [word]
                transition = 1
            elif item[i] != ' ' and item[i].isalpha():
                word += item[i]
            elif item[i].isnumeric():
                new_integer += item[i]
        list2 += [int(new_integer)]

    for i in range(len(list1)):
        new_tuple = list1[i],list2[i]
        new_list += [new_tuple]
        new_tuple = []
    return new_list

# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()

# d15606d9770cfd538b6d7c9c66cd23faa4bcb65a84818606563688257a8582d3 pfnenucynpxr
