#!/usr/bin/env python3
"""
GR1, CS210, Fall 2017
"""
import math
import random
import easygui
import turtle

# Fill in the metadata
__author__ = "Sarah Placke "  # Your name. Ex: John Doe
__section__ = "T6A"  # Your section. Ex: M1
__version__ = "C"  # Test version. Ex: A
__date__ = "20 Sep 2017"  # Today's date. Ex: 25 Dec 2017

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 // 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH // 30  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = MARGIN // 2  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, non-animated turtle movement.

""" The following code segments are provided for your reference.
    You are free to copy/paste them into your answers below.

    # Use the writing turtle to write a message, centered at its location, in a bold font.
    writer.write( "Hello, World!", align="center", font=( "Courier", FONT_SIZE, "bold" ) )

    # An easygui message box to display a formatted string.
    easygui.msgbox( "pi = {:.2f}".format( 22 / 7 ), "Result" )

    # An easygui enter box for entering a string.
    s = easygui.enterbox( "Enter a string:", "Input" )

    # An easygui integer box for entering a positive integer.
    n = easygui.integerbox( "Enter a positive integer:", "Input", 42, 1, 2 ** 31 )

    # Read the entire contents of a file into a single string.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_string = data_file.read()

    # Read the entire contents of a file into a list of strings, one per word.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_words = data_file.read().split()

    # Read the entire contents of a file into a list of strings, one per line.
    with open( easygui.fileopenbox( default="../Data/*.txt" ) ) as data_file:
        data_lines = data_file.read().splitlines()
"""


# ======================================================================
# Main
# ======================================================================
def main():
    """ Comment and uncomment these lines as needed to work on each problem. """
    #problem1()
    #problem2()
    #problem3()
    problem4()
    #problem5()


# ======================================================================
# Problem 1                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem1():
    """Problem 1 from the GR."""
    print_problem_name()

    # TODO 1b
    print("Barleycorn \t Inches")
    print("========== \t =======")
    for i in range(1,10):
        inches = barleycorn_to_inches(i)
        print ("{:<10}\t {:<10.2f}".format(i,inches))

# TODO 1a
def barleycorn_to_inches(number):
    inches = number/3
    return inches

# ======================================================================
# Problem 2                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem2():
    """Problem 2 from the GR."""
    print_problem_name()
    import easygui
    # TODO 2b
    rolls = easygui.integerbox("Enter a number of rolls between 1 and 100","Input",lowerbound=1,upperbound=100)
    target = easygui.integerbox("Please enter a target value between 2 and 12","Input",lowerbound=2,upperbound=12)
    result = count_dice(rolls, target)
    easygui.msgbox("In {} rolls, a {} came up {} times.".format(rolls,target,result),"Tally")


# TODO 2a
def count_dice(rolls,target):
    counter = 0
    for i in range(rolls):
        value1 = random.randint(1,6)
        value2 = random.randint(1,6)
        value = value1+value2
        if value == target:
            counter = counter+1
    return counter

# ======================================================================
# Problem 3                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# Including 5 pts for good docstring
# ======================================================================
def problem3():
    """Problem 3 from the GR."""
    print_problem_name()

    # TODO 3b
    value1 = easygui.integerbox("Please enter a positive integer","GCD", default=42)
    value2 = easygui.integerbox("Please enter a positive integer","GCD", default=28)
    value3 = easygui.integerbox("Please enter a positive integer","GCD", default=63)
    value4 = easygui.integerbox("Please enter a positive integer","GCD", default=84)

    while value1 != value2:
        value1 = gcd(value1, value2)
        value2 = gcd(value2, value3)

    while value3 != value4:
        value3 = gcd(value3, value4)
        value4 = gcd(value4, value1)

    result = gcd (value3, value1)
    easygui.msgbox("The greatest common divisor is {}.".format(value1,value2,value3,value4, result))





# TODO 3a
def gcd(value1,value2):
    """Euclid's algorithm for greatest common denominator (Larger value gets smaller until values equal)
    :param int value1: One of the multiples of the greatest common denominator
    :param int value2: One of the multiples of the greatest common denominator
    :return: greatest common denominator
    :type: int
    """
    while value1 != value2:
        if value1 > value2:
            value1 = value1 - value2
        else:
            value2 = value2 - value1
    return value1

# ======================================================================
# Problem 4                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem4():
    """Problem 4 from the GR."""
    print_problem_name()
    # Reading in a file followed by split() creates a list of words
    # such as you have used in labs. For example:
    # data_words = ["It", "was", "the", "worst", "of", "times"]
    with open(easygui.fileopenbox(default="../Data/*.txt")) as data_file:
        data_words = data_file.read().split()

    # TODO 4b (no longer indented under the with statement above)

    first = count_word("we",data_file)
    first1 = count_word("us",data_file)
    total_first = first+first1

    third = count_word("they",data_file)
    third1 = count_word("them",data_file)
    total_third = third+third1

    if total_first>total_third:
        easygui.msgbox("There were more first person than third person pronouns",'OK')
    if total_first < total_third:
        easygui.msgbox("There were more third person than first person pronouns",'OK')
    if total_first == total_third:
        easygui.msgbox("There are equal third person and first person pronouns",'OK')




# TODO 4a
def count_word(word,list):
    counter = 0
    for i in list:
        if i == word:
            counter = counter+1
    return counter

# ======================================================================
# Problem 5                                                ____ / 25 pts
# Grades: A: 25, B: 20, C: 17, D: 15, F: 12
# ======================================================================
def problem5():
    """Problem 6 from the GR."""
    print_problem_name()

    # Create the turtle screen and two turtles
    screen, artist, writer = turtle_setup()
    big_radius = (HEIGHT-2*MARGIN)/2
    small_radius= (HEIGHT-2*MARGIN)/4
    # TODO 5
    artist.penup()
    artist.setposition(0,-HEIGHT*0.5+MARGIN)
    artist.pendown()
    artist.circle(big_radius)
    artist.penup()
    artist.setposition(0,-(HEIGHT-2*MARGIN)/4)
    artist.pendown()
    artist.circle(small_radius)



    shots = 0
    misses = 0
    points = 0
    while shots < 3 and misses <2:
        x = random.randint(-WIDTH*0.5,0.5*WIDTH)
        y = random.randint(-HEIGHT*0.5,HEIGHT*0.5)
        artist.penup()
        artist.setposition(x,y)
        artist.pendown()
        if x < small_radius and x> -small_radius and y< small_radius and y > -small_radius:
            artist.dot(MARGIN,"Green")
            points = points+2
        elif x < big_radius and x > -big_radius and y < big_radius and y > -big_radius:
            artist.dot(MARGIN,"Yellow")
            points = points+1
        else:
            artist.dot(MARGIN,"Red")
            misses = misses + 1
        shots = shots + 1
    if misses>=2 and shots < 3:
        writer.setposition(-MARGIN+60,MARGIN)
        writer.write("Too many misses.\nTry Again.",align = "center")
    if shots >= 3:
        writer.setposition(-MARGIN+30,MARGIN)
        writer.write("Shots Taken: {}\nFinal Score: {}".format(shots,points),align="center")

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_problem_name():
    """Print the name and docstring of the calling function (i.e., the current problem.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        doc_color = "\033[91m" if doc is None else "\033[92m"
        print('\n\033[94m{}\n{}\n{}{}\033[99m'.format(name, "=" * len(name), doc_color, doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:",
          (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Test Version:",
          (__version__ if __version__ else "\033[91mBLANK (You must fill in the __version__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64
        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# d15606d9770cfd538b6d7c9c66cd23faa4bcb65a84818606563688257a8582d3 pfnenucynpxr
