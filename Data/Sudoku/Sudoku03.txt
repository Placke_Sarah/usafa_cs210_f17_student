# A sample, solved puzzle.
# This is a solved version of puzzle 03b.
1 8 4 6 3 9 2 5 7
5 9 7 1 2 4 6 8 3
6 3 2 5 8 7 4 1 9
7 6 9 8 4 5 3 2 1
4 5 3 2 7 1 9 6 8
2 1 8 9 6 3 7 4 5
3 7 1 4 5 2 8 9 6
8 2 5 7 9 6 1 3 4
9 4 6 3 1 8 5 7 2
