#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

import turtle

__author__ = "C3C Sarah Placke"  # Your name. Ex: John Doe
__section__ = "T6"  # Your section. Ex: M1
__instructor__ = "Maj Sievers"  # Your instructor. Ex: Lt Col Doe
__date__ = "29 Aug 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = "I went to EI to receive help on fixing the length of the red line. This was a simple math area based on " \
                    "the principle that LENGTH = .5 of most of the objects' actual length.""  """  # Multiple lines OK with triple quotes

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True # Set to True for fast, stealthy turtles.
LENGTH = ((WIDTH/2)-(2*MARGIN))*(1/3)
import math
def main():
    # Create the turtle screen and two turtles (leave this as the first line of main).
    screen, artist, writer = turtle_setup()

    # TODO: Replace this hello world code with your code
    # Draw a circle and say hello (remove these lines and implement your own turtle code here).
    #DRAW TRIANGLE
    artist.penup()
    artist.setposition((-WIDTH / 2) + MARGIN + 2 * LENGTH, -LENGTH)
    artist.pendown()
    artist.left(135)
    artist.forward(math.sqrt(((LENGTH * 2) ** 2) + ((LENGTH * 2) ** 2)))
    artist.left(135)
    artist.forward(LENGTH * 2)
    artist.left(90)
    artist.forward(LENGTH * 2)

    #DRAW SQUARE
    artist.penup()
    artist.setposition(LENGTH,-LENGTH)
    artist.pendown()
    for i in range (4):
        artist.left(90)
        artist.forward(LENGTH*2)

    #DRAW CIRCLE
    artist.penup()
    artist.setposition((WIDTH / 2) - MARGIN - LENGTH, -LENGTH)
    artist.pendown()
    artist.circle(LENGTH)

    #inputs fill percentage
    fill = screen.numinput("Fill","Your fill percent:", 80, minval=50, maxval=95)
    artist.penup()
    artist.setposition(0,LENGTH+50)

    # showing input fill percent
    artist.write('Fill Percentage:{}%'.format(fill),True,align="center",font=("Arial",15,"normal"))


    #Draw the Fill Line
    artist.pencolor("red")
    x = (-WIDTH/2)+MARGIN,-LENGTH,WIDTH/2-MARGIN-LENGTH #beginning of line
    y = (2*LENGTH*fill/100)-LENGTH #height of line

    # calculate length of line
    line =(2*LENGTH-2*LENGTH*fill/100),LENGTH*2,math.sqrt((LENGTH**2)-(((fill/100*2*LENGTH)-LENGTH)**2))

    # drawing red line
    for a in range(len(x)):
        artist.penup()
        artist.setposition(x[a],y)
        artist.pendown()
        artist.forward(line[a])

    #Draws the other half of the redline (circle)
    artist.penup()
    artist.setposition(WIDTH/2-MARGIN-LENGTH,fill*LENGTH/50-LENGTH)
    artist.pendown()
    artist.backward(math.sqrt((LENGTH**2)-(((fill/100*2*LENGTH)-LENGTH)**2)))

    #Whole Areas
    TRIANGLEA =.5*((2*LENGTH)**2)
    SQUAREA =(2*LENGTH)**2
    CIRCLEA = math.pi * (LENGTH ** 2)


    # area of the top of a circle
    theta = math.sqrt((LENGTH**2)-(((fill/100*2*LENGTH)-LENGTH)**2))/LENGTH
    TOPCIRCLEA = .5*(LENGTH**2)*(2*math.asin(theta)-math.sin(2*math.asin(theta)))

    # Area underneath red line

    TA = TRIANGLEA-(.5*((2*LENGTH-2*LENGTH*fill/100)**2))
    SA = SQUAREA*fill/100
    CA = CIRCLEA-TOPCIRCLEA

    #Writing the Area
    artist.pencolor("black")
    artist.penup()
    artist.setposition(-2*LENGTH-MARGIN,-LENGTH-50)
    artist.write("{:,.2f}".format(TA),True,align="center",font=("Arial",15,"normal"))
    artist.penup()
    artist.setposition(0,-LENGTH-50)
    artist.write("{:,.2f}".format(SA), True, align="center", font=("Arial", 15, "normal"))
    artist.penup()
    artist.setposition(2 * LENGTH + MARGIN, -LENGTH - 50)
    artist.write(("{:,.2f}").format(CA), True, align="center", font=("Arial", 15, "normal"))

    screen.exitonclick()
# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    b = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    try:
        import base64

        eval(compile(base64.b64decode(b), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# d15606d9770cfd538b6d7c9c66cd23faa4bcb65a84818606563688257a8582d3 pfnenucynpxr
