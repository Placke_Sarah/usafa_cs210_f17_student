#!/usr/bin/env python3
"""
PEX 1: Dice Game Creation using the Easygui and Turtle Modules. Includes random dice sides, score keeping, and dice drawing.
CS 210, Introduction to Programming
"""

import easygui
import random
import turtle

__author__ = "Sarah Placke"  # Your name. Ex: John Doe
__section__ = "T6"  # Your section. Ex: M1
__instructor__ = "Maj Sievers"  # Your instructor. Ex: Lt Col Doe
__date__ = "17 Sep 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """None  """  # Multiple lines OK with triple quotes

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 960  # Usually 720, 960, 1024, 1280, 1600, or 1920.
HEIGHT = WIDTH * 9 / 16  # Produces the eye-pleasing 16:9 HD aspect ratio.
MARGIN = WIDTH/16  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, stealthy turtles.
import easygui

def main():
    # Create the turtle screen and two turtles (leave this as the first line of main).
    screen, artist, writer = turtle_setup()

    player1 = easygui.enterbox("Please enter Player 1:","Welcome",default="Anna")   #establishes player names
    player2 = easygui.enterbox("Please enter Player 2:","Welcome",default="Alex")
    score1=0
    score2=0
    while score1 < 100 and score2 < 100:     #ensures that the program continues running until someone reaches the required score
        score1 = take_one_turn(artist, player1, score1,1)
        scoreboard(writer,player1,1,score1)
        score2 = take_one_turn(artist, player2, score2,2)
        scoreboard(writer,player2,2,score2)
    if score1>=100 and score2<100:           # if one player wins without debate
        clear_dice(artist,"Light Blue")
        writer.setposition(0,-1.25*MARGIN)
        writer.write("WINNER WINNER CHICKEN DINNER!\n        CONGRATULATIONS {}".format(player1),True,align="center",
                     font=('Arial',30,'normal'))
    if score2>=100 and score1<100:
        clear_dice(artist, "Light Blue")
        writer.setposition(0, -1.25 * MARGIN)
        writer.write("WINNER WINNER CHICKEN DINNER!\n        CONGRATULATIONS {}".format(player2), True, align="center",
                     font=('Arial', 30, 'normal'))
    if score1>=100 and score2>=100:          #Condition if both players exceed/reach 100 in the same turn
        clear_dice(artist, "Light Blue")
        easygui.msgbox("Wow! You both have the luck of the roll. However, this is war. There can be only one winner. "
                       "These rolls determine the victor",'OK')
        last_time=0
        while last_time ==0:                #One more roll for each. The one with the most pips wins. If same number, it rolls again.
            final1=random.randint(1,6)
            draw_die(artist,final1,-3.6*MARGIN,2*MARGIN,"SkyBlue")
            final2=random.randint(1,6)
            draw_die(artist,final2,2.4*MARGIN,2*MARGIN,"Pink")
            if final1>final2:
                writer.setposition(0, -1.25 * MARGIN)
                writer.write("WINNER WINNER CHICKEN DINNER!\n        CONGRATULATIONS {}".format(player1), True,
                             align="center",font=('Arial', 30, 'normal'))
                last_time=1
            if final2>final1:
                writer.setposition(0, -1.25 * MARGIN)
                writer.write("WINNER WINNER CHICKEN DINNER!\n        CONGRATULATIONS {}".format(player2), True,
                             align="center",font=('Arial', 30, 'normal'))
                last_time=1








    # Wait for the user to click before closing the window (leave this as the last line of main).
    screen.exitonclick()


def take_one_turn(artist, player, score,player_number):
    """ Updates screen with dice, offers choices of play, updates score, specifies color dice for player (player_number)

    :param turtle artist: turtle being used
    :param str player: current player playing
    :param int score: current score of player
    :param int player_number: whether it is player (1) or player (2)
    :return: new total score for the player
    :type: int
    """
    end = 0
    clear_dice(artist,"Light Blue")

    x = -WIDTH*0.5+0.8*MARGIN
    y = HEIGHT*0.5-2.4*MARGIN
    counter=0
    turn_score=0
    choice = True

    while end == 0:

        if choice == True:                  #Allows for dice to continue being drawn until end of turn
            pips = random.randint(1,6)
            if player_number%2==0:
                draw_die(artist,pips,x,y,"Pink")
            if player_number%2!=0:
                draw_die(artist,pips,x,y,"SkyBlue")
            if counter<8:                   # Moves dice so that they are drawn in a row
                x = x+1.9*MARGIN
                counter=counter+1
            if counter>=8:                  # Moves dice so that a second or more row can be drawn if necessary
                x = -WIDTH*0.5+0.8*MARGIN
                y = y-2*MARGIN
                counter=0

            if pips==1:                     # Changes and ends turn score if a one is rolled
                turn_score=0
                end = 1
                easygui.msgbox("Ouch! Your score is now zippittee nothin'","Ow...",'OK')
            else:
                turn_score=turn_score+pips          #Updates turn score each turn
                if score+turn_score>=100:           #Ends turn if 100 is reached
                    end = 1
            if pips != 1:                           # Re-asks question
                choice = easygui.boolbox("Choose Wisely {}. Your score for this turn is currently {}.".format(player, turn_score), "Game", choices=['[R]oll', '[H]old'])
        else:
            end = 1                           #ends turn


    score = score + turn_score                #updates total score
    return score                              #returns score meant to be updated/drawn




def draw_die(tom, pips, x, y,color):
    """ Randomly assigns number of pips, draws dice.

    :param turtle tom: specifies the turtle module used
    :param int pips: tells how many pips to draw
    :param float x: tells bottom left x-coordinate of die (location)
    :param float y : tells bottom left y-coordinate of die (location)
    :param str color: tells what color to draw the pips and the die



    """
    tom.penup()
    tom.setposition(x,y)                    #sets bottom left corner of cube
    tom.pendown()
    if color == "SkyBlue":                  #specifies background color of dice (dice are player-specific colored)
        tom.color("Blue")
    if color == "Pink":                     #specifies background color of dice
        tom.color("Red")
    tom.begin_fill()
    for i in range(4):                      #outline of dice
        tom.forward(MARGIN)                 #straight edges of dice
        tom.circle(MARGIN*0.1,90)           #curved corners of dice
    tom.end_fill()
    tom.color(color)
    if pips%2==1:
        tom.penup()
        tom.setposition(x+MARGIN*0.5,y+MARGIN*0.5)
        tom.pendown()
        tom.begin_fill()
        tom.circle(MARGIN*0.1)
        tom.end_fill()
    if pips>1:
        tom.penup()
        tom.setposition(x+MARGIN*0.2,y+MARGIN*0.8)
        tom.pendown()
        tom.begin_fill()
        tom.circle(MARGIN*0.1)
        tom.end_fill()
        tom.penup()
        tom.setposition(x+MARGIN*0.8,y+MARGIN*0.2)
        tom.pendown()
        tom.begin_fill()
        tom.circle(MARGIN*0.1)
        tom.end_fill()
    if pips>3:
        tom.penup()
        tom.setposition(x + MARGIN * 0.8, y + MARGIN * 0.8)
        tom.pendown()
        tom.begin_fill()
        tom.circle(MARGIN * 0.1)
        tom.end_fill()
        tom.penup()
        tom.setposition(x + MARGIN * 0.2, y + MARGIN * 0.2)
        tom.pendown()
        tom.begin_fill()
        tom.circle(MARGIN * 0.1)
        tom.end_fill()
    if pips==6:
        tom.penup()
        tom.setposition(x+MARGIN*0.2,y+MARGIN*0.5)
        tom.pendown()
        tom.begin_fill()
        tom.circle(MARGIN*0.1)
        tom.end_fill()
        tom.penup()
        tom.setposition(x+MARGIN*0.8, y+MARGIN*0.5)
        tom.pendown()
        tom.begin_fill()
        tom.circle(MARGIN*0.1)
        tom.end_fill()


def scoreboard(turtle,player,player_number,score):
    """Writes/Updates scores

    :param turtle turtle: specifies which turtle to use
    :param str player: specifies who's score it is
    :param int player_number: specifies player (used for determining where to write score
    :score int score: inputs score to write """
    writingx = -4*MARGIN
    writingy = HEIGHT * 0.5 - 0.75*MARGIN

    if player_number%2==0:              #determines who's score is being updated
        turtle.setposition(writingx + 4*MARGIN, writingy)
        turtle.color("SkyBlue")
        turtle.begin_fill()
        while True:
            for i in range(4):          #draws square of background color to clear screen, so numbers will not write over one another
                turtle.forward(4 * MARGIN)
                turtle.right(90)
            break
        turtle.end_fill()
        turtle.color("Red")
        turtle.setposition(writingx + 6*MARGIN, writingy)
        turtle.write("{}:{}".format(player, score), True, align="left",font=('Arial',20,'normal'))



    else:
        turtle.setposition(writingx - 2*MARGIN, writingy)
        turtle.color("SkyBlue")
        turtle.begin_fill()
        while True:
            for i in range(4):             #clearing box
                turtle.forward(5 * MARGIN)
                turtle.right(90)
            break
        turtle.end_fill()
        turtle.color("Blue")
        turtle.setposition(writingx, writingy)
        turtle.write("{}:{}".format(player, score), False,align = "Left",font=('Arial',20, 'normal'))


def clear_dice(artist,color):
    """ clears screen of the previous turn's dice

    :param turtle artist: which turtle is used
    :param str color: what color to fill box (background color)"""
    artist.color(color)
    artist.penup()
    artist.setposition(-WIDTH * 0.5, -HEIGHT * 0.5)
    artist.pendown()
    artist.begin_fill()
    for i in range(2):                  #draws rectangle so the score is not altered
        artist.forward(WIDTH)
        artist.left(90)
        artist.forward(HEIGHT - MARGIN)
        artist.left(90)
    artist.end_fill()




# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    #  ___   ___     _  _  ___ _____    __  __  ___  ___ ___ _____   __
    # |   \ / _ \   | \| |/ _ \_   _|  |  \/  |/ _ \|   \_ _| __\ \ / /
    # | |) | (_) |  | .` | (_) || |    | |\/| | (_) | |) | || _| \ V /
    # |___/ \___/   |_|\_|\___/ |_|    |_|  |_|\___/|___/___|_|   |_|
    #  _____ _  _ ___ ___    ___ _   _ _  _  ___ _____ ___ ___  _  _
    # |_   _| || |_ _/ __|  | __| | | | \| |/ __|_   _|_ _/ _ \| \| |
    #   | | | __ || |\__ \  | _|| |_| | .` | (__  | |  | | (_) | .` |
    #   |_| |_||_|___|___/  |_|  \___/|_|\_|\___| |_| |___\___/|_|\_|
    #
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    b = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    try:
        import base64

        eval(compile(base64.b64decode(b), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# d15606d9770cfd538b6d7c9c66cd23faa4bcb65a84818606563688257a8582d3 pfnenucynpxr
