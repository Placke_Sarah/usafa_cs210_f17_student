#!/usr/bin/env python3
"""
PEX 5- Code Template
Mitches Give You Glitches
CS 210, Introduction to Programming
"""

import tkinter as tk
from PIL import Image, ImageTk
import pygame
import random
import os  # For os.path.basename.

__author__ = "Sarah Placke and Allison Raines"
__section__ = "T6/7"
__instructor__ = "Maj Sievers"
__date__ = "16 Nov 2017"
__documentation__ = """ Used templates from PEX3 and PEX4 """


def main():
    """
    Main program to run Mitches video game
    """
    pygame.init()

    clock = pygame.time.Clock()
    FPS = 30
    program = Mitches()
    while True:
        clock.tick(FPS)
        pygame.display.update()



def end_game(player):
    """
    This method will be called after a player has finished the game.
    It shall update the screen and internal variables as needed.
    :param Player player: my player object
    """


class Player:
    """ A player in the Mitches game. """

    def __init__(self, name="Player"):
        """
        Initializes a new Player with a name, health and meals.
        :param str name: the player's name
        """
        self.__name = name
        self.__health_level = 100
        self.__meals = 0

    @property
    def name(self):
        """
        Provides read-access to the name property.
        :return: player's name
        :rtype: str
        """
        return self.__name

    @property
    def health_level(self):
        """
        Provides read-access to the health property.
        Key: 0 = dead, 1 = poor, 2 = average, 3 = good, 4 = excellent
        :return: player's health
        :rtype: int
               """
        return self.__health_level

    @health_level.setter
    def health_level(self, new_health):
        """
        Provides write-access to the health property.

        If a health is set that is longer that is less than 0 or greater than 4,
        print to the console to let the programmer know they are accessing this
        property in the incorrect way.
        :param int new_health: the new health rating
        """
        self.__health_level = new_health

    @property
    def meals(self):
        """
        Provides read-access to the meals property.
        :return: player's meals (out of 5 levels)
        :rtype: list
        """
        return self.__meals

    @meals.setter
    def meals(self, next_level):
        """
        Provides write-access to the meals property.

        If the meals property is already 5, there is no greater level.
        Print to the console to let the programmer know they are accessing
        this property in the incorrect way.
        :param bool next_level: whether to move on to the next meal.
        """
        if next_level == True:
            if self.__meals == 10:
                end_game(self)
            else:
                self.__meals += 1
        else:
            end_game(self)

    def up_level(self):
        """
        This method will be called after a player makes a food choice that raises
        their health to 4, excellent. They move onto the next meal/level.
        It shall update internal variables as needed.
        """


class Mitches:
    """ The Mitches game console. """

    def __init__(self):
        """
        Initializes a new Mitches game with dictionary of food names:files.
        """
        self.__food = {"breakfast": ["Waffles.png", "Bacon.png", "Breakfast Burrito.png", "Eggs.png", "Biscuits.png", "Ham.PNG"], "lunch": [
            "Cornbread.png", "Fish.png", "Pasta.png", "Mac n' Cheese.png", "Chicken.png", "Rice.png"],
                       "dinner": ["Steak.png", "Turkey.png", "Stir Fry.png", "Soup.png", "Salad.png", "Broccoli.png"]}
        pygame.display.init()
        self.window = pygame.display.set_mode((1000, 1000))
        title = "Mitches Gives You Glictches"
        pygame.display.set_caption(title)
        BLACK = (0, 0, 0)
        WHITE = (255, 255, 255)
        counter = 0
        clock = pygame.time.Clock()
        pygame.font.init()
        crashed = False
        WHITE = (255, 255, 255)
        DARKBLUE = (0, 0, 139)
        BLUE = (0, 0, 255)
        GRAY = (192, 192, 192)
        GREEN = (0, 255, 0)
        RED = (255, 0, 0)
        pygame.draw.rect(self.window, DARKBLUE, (0, 750, 1000, 250), 0)
        plane = pygame.image.load('drone1.jpg')
        plane = pygame.transform.scale(plane, (1000, 300))
        self.window.blit(plane, (0, 0))
        pygame.draw.rect(self.window, BLUE, (0, 300, 1000, 450), 0)
        pygame.draw.rect(self.window, GRAY, (100, 390, 200, 70), 0)
        pygame.draw.rect(self.window, GRAY, (400, 430, 200, 70), 0)
        pygame.draw.rect(self.window, GRAY, (700, 390, 200, 70), 0)
        pygame.draw.rect(self.window, WHITE, (700, 0, 300, 50), 0)
        pygame.draw.rect(self.window, RED, (700, 0, 300, 50), 0)
        mouse = pygame.image.load('mouse.png')
        mouse = pygame.transform.scale(mouse, (150, 125))
        self.window.blit(mouse, (700, 560))
        plate = pygame.image.load('Plate2.png')
        plate = pygame.transform.scale(plate, (200, 170))
        self.window.blit(plate, (400, 570))
        myfont = pygame.font.SysFont('Broadway', 100)
        textsurface = myfont.render('Mitches Gives', False, WHITE)
        self.window.blit(textsurface, (130, 760))
        textsurface1 = myfont.render('You', False, WHITE)
        self.window.blit(textsurface1, (130, 890))
        myfont = pygame.font.SysFont('SWTxt', 100)
        text = myfont.render('Glitches', False, GREEN)
        self.window.blit(text, (370, 880))
        pygame.display.update()

        while not crashed:
            for event in pygame.event.get():
               if event.type == pygame.QUIT:
                    crashed = True
            clock.tick(60)
            for meal in self.__food:
                self.update_background(meal)
        pygame.display.quit()
        pygame.quit()


    @property
    def food(self):
        """
        Provides read-access to the food property.
        :return: game's dictionary of food
        :rtype: dict
        """
        return self.__food

    def update_background(self,meal_type):
        """
        Prints the table as a background and randomly selects 6 food
        items from the dictionary. Prints the corresponding food images.
        Updates health meter and game information (meals left, game level).
        """
        x = 100
        y =350
        GRAY = (192, 192, 192)
        used = []
        while len(used) < 3:
             item = random.randint(0,5)
             if item not in used:
                if len(used) == 1:
                     y = y + 50
                pygame.draw.rect(self.window, GRAY, (x, y, 200, 50), 0)
                for i in range(4):
                    food = pygame.image.load(self.__food[meal_type][item])
                    food = pygame.transform.scale(food, (50, 50))
                    self.window.blit(food, (x, y))
                    pygame.display.update()
                    x += 50
                if len(used) == 1:
                    y = y - 50
                used += [item]
                x += 100






    ############################
    def create_buttons(self, food_choices):
        """
        Implements GUI functionality to display buttons. The buttons will
        correspond to the randomly selected food names.

        :param list food_choices: The list of names of selected food options.
        """


if __name__ == "__main__":
    main()
