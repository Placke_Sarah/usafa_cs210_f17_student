#!/usr/bin/env python3
"""
PEX 5- Mitches Give You Glitches
CS 210, Introduction to Programming
"""

import tkinter as tk
import os  # For os.path.basename.

__author__ = "Sarah Placke and Allison Raines"
__section__ = "T6/7"
__instructor__ = "Maj Sievers"
__date__ = "16 Nov 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ Used templates from PEX3 and PEX4 """  # Multiple lines OK with triple quotes


def main():
    """
    Main program to run Mitches video game
    """


def end_game(self, player):
    """
    This method will be called after a player has finished the game.
    It shall update the screen and internal variables as needed.
    :param Player object
    """


class Player:
    """ A player in the Mitches game. """

    def __init__(self, name="Player"):
        """
        Initializes a new Player with a name, health and meals.
        :param str name: the player's name
        """
        self.__name = name  # default is 'Player'
        self.__health = 2  # 2 = average
        self.__meals = 1  # 1 out of 5 rounds

    @property
    def name(self):
        """
        Provides read-access to the name property.
        :return: player's name
        :rtype: str
        """
        return self.__name

    @property
    def health(self):
        """
        Provides read-access to the health property.
        Key: 0 = dead, 1 = poor, 2 = average, 3 = good, 4 = excellent
        :return: player's health
        :rtype: int
               """
        return self.__health

    @health.setter
    def health(self, new_health):
        """
        Provides write-access to the health property.

        If a health is set that is longer that is less than 0 or greater than 4,
        print to the console to let the programmer know they are accessing this
        property in the incorrect way.
        :param int new_health: the new health rating
        """

    @property
    def meals(self):
        """
        Provides read-access to the meals property.
        :return: player's meals (out of 5 levels)
        :rtype: int
        """
        return self.__meals

    @meals.setter
    def meals(self, next_level):
        """
        Provides write-access to the meals property.

        If the meals property is already 5, there is no greater level.
        Print to the console to let the programmer know they are accessing
        this property in the incorrect way.
        :param bool next_level: whether to move on to the next meal.
        """

    def next_level(self):
        """
        This method will be called after a player makes a food choice that raises
        their health to 4, excellent. They move onto the next meal/level.
        It shall update internal variables as needed.
        """
        self.health = 3  # reset health to average
        if self.meals < 5:
            self.meals += 1  # reset to the next meal


class Mitches:
    """ The Mitches game console. """

    def __init__(self):
        """
        Initializes a new Mitches game with dictionary of food names:files.
        """
        self.__food = {}

    @property
    def food(self):
        """
        Provides read-access to the food property.
        :return: game's dictionary of food
        :rtype: dict
        """
        return self.__food

    def update_background(self):
        """
        Prints the table as a background and randomly selects 6 food
        items from the dictionary. Prints the corresponding food images.
        Updates health meter and game information (meals left, game level).
        """
        food_choices = []
        # TODO 2a: Create a list of randomly selected food names and send to create_buttons.
        self.create_buttons(food_choices)

    def create_buttons(self, food_choices):
        """
        Implements GUI functionality to display buttons. The buttons will
        correspond to the randomly selected food names.

        :param list food_choices: The list of names of selected food options.
        """
