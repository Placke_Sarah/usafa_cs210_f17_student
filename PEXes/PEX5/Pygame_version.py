#!/usr/bin/env python3
"""
PEX 5- Code Template
Mitches Give You Glitches
CS 210, Introduction to Programming
"""

import tkinter as tk
from PIL import Image, ImageTk
import pygame, sys
import random
import time
import os  # For os.path.basename.

__author__ = "Sarah Placke and Allison Raines"
__section__ = "T6/7"
__instructor__ = "Maj Sievers"
__date__ = "16 Nov 2017"
__documentation__ = """ Used templates from PEX3 and PEX4 """


def main():
    """
    Main program to run Mitches video game
    """
    #Initializes Pygame/ Classes
    pygame.init()
    player = Player()
    program = Mitches(player)




def end_game(player, game):
    """
    This method will be called after a player has finished the game.
    It shall update the screen and internal variables as needed.
    :param Player player: my player object
    :param Mitches game: my mitches object
    """
    # print game over screen
    game_over = pygame.image.load('game_over.jpg')
    game.window.blit(game_over, (0, 0))

    # print score
    myfont = pygame.font.SysFont('Broadway', 50)
    BLACK = (0, 0, 0)
    textsurface = myfont.render('You survived {} meals at Mitches'.format(player.meals), False, BLACK)
    game.window.blit(textsurface, (70, 800))
    pygame.display.update()
    time.sleep(3)
    #Exits loop and Closes Window
    game.crashed = True





class Player:
    """ A player in the Mitches game. """

    def __init__(self, name="Player"):
        """
        Initializes a new Player with a name, health and meals.
        :param str name: the player's name
        """
        self.__name = name
        self.__health_levels = ['Dead','Poor','Average', 'Good', 'Excellent']
        self.__health_level = 2
        self.__meals = 0

    @property
    def name(self):
        """
        Provides read-access to the name property.
        :return: player's name
        :rtype: str
        """
        return self.__name

    @property
    def health_level(self):
        """
        Provides read-access to the health property.
        Key: 0 = dead, 1 = poor, 2 = average, 3 = good, 4 = excellent
        :return: player's health
        :rtype: int
               """
        return self.__health_level

    @health_level.setter
    def health_level(self, args):
        """
        Provides write-access to the health property.

        If a health is set that is longer that is less than 0 or greater than 4,
        print to the console to let the programmer know they are accessing this
        property in the incorrect way.
        :param tuple args: the new health rating
        """
        #First arguement dictates what type of food was selected. If a bad food was chosen, the health level decreases
        if args[0] == "bad":
            self.__health_level = self.__health_level - 1
            #Checks if the health level reaches zero. If so, the game is ended
            if self.__health_level == 0:
                end_game(self, args[1])
        else:
            #If the health level is already maximized, it will remain at that level until a poor choice is made.
            if self.__health_level == 4:
                self.__health_level = 4
            else:
                self.__health_level += 1
        #Second argument transfers the actual game, so that the end game function can update the end-game photo/info
        #to the game window
        self.meals = args[1]


    @property
    def meals(self):
        """
        Provides read-access to the meals property.
        :return: player's meals (out of 10 levels)
        :rtype: list
        """
        return self.__meals

    @meals.setter
    def meals(self, game):
        """
        Provides write-access to the meals property.

        If the meals property is already 10, there is no greater level.
        Print to the console to let the programmer know they are accessing
        this property in the incorrect way.
        :param bool next_level: whether to move on to the next meal.
        :param Mitches game: Mitches game
        """
        if self.__meals == 10:
            end_game(self, game)
        else:
            self.__meals += 1





class Mitches:
    """ The Mitches game console. """

    def __init__(self,player):
        """
        Initializes a new Mitches game with dictionary of food names:files.
        """
        #Dictionary of food --- classifies poor choices and good choices
        self.__food = {"good": ["Waffles.png", "Bacon.png", "Breakfast Burrito.png", "Biscuits.png", "Cornbread.png",
                                "Pasta.png", "Chicken.png", "Rice.png","Stir Fry.png" ],
                       "bad": ["Steak.png", "Turkey.png", "Soup.png", "Salad.png", "Broccoli.png", "Eggs.png",
                               "Fish.png", "Ham.PNG", "Mac n' Cheese.png"]}
        self.__player = player

        #Specifies which image to post for the different poisons in the food
        self.__poisons = ["salmonella.jpg", "parasites.jpg", "virus.jpg", "e.coli.jpg", "fecal_matter.jpg"]
        #Pictures of each health level bar
        self.__health_levels = ['level_dead.png','level_poor.png','level_average.png', 'level_good.png', 'level_excellent.png']
        #Initializes the display/Opens Window
        pygame.display.init()
        self.window = pygame.display.set_mode((1000, 1000))
        #Sets the Title
        title = "Mitches Gives You Glitches"
        pygame.display.set_caption(title)
        self.draw_original_background()
        #Draws the initial "average" health bar
        health = pygame.image.load('level_average.png')
        health = pygame.transform.scale(health, (500, 75))
        self.window.blit(health, (500, 0))
        # Initializes condition that will allow running loop to continue/start
        self.__crashed = False

        while not self.__crashed:
            self.update_background()
            #Checks for someone pressing the x-button and changes the condition accordingly
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.__crashed = True


        pygame.quit()








    @property
    def crashed(self):
        return self.__crashed

    @crashed.setter
    def crashed(self,new):
        self.__crashed = new
    @property
    def player(self):
        return self.__player

    @property
    def food(self):
        """
        Provides read-access to the food property.
        :return: game's dictionary of food
        :rtype: dict
        """
        return self.__food

    @property
    def poisons(self):
        """
        Provides read-access to the food property.
        :return: game's dictionary of food
        :rtype: dict
                """
        return self.__poisons

    def update_background(self):
        """
        Prints the table as a background and randomly selects 6 food
        items from the dictionary. Prints the corresponding food images.
        Updates health meter and game information (meals left, game level).
        """
        #Initializes upper left hand corner of first meal choice photo
        x = 70
        y =350
        #Initializes Font
        myfont = pygame.font.SysFont('Broadway', 20)
        #Colors for later use with graphics
        GRAY = (192, 192, 192)
        WHITE = (255,255,255)
        BLUE = (0,0,255)
        #Initializes empty lists that will be used to count the amount of selections and their corresponding types
        meal_type = ["bad", "good"]
        used = []
        meal_choices = []
        names = []
        #Ensures 4 options are chosed
        while len(used) < 4:
            #Used to ensure that there are an equal amount of good and bad choices but in random order
             meal_choice = meal_type[random.randint(0,1)]
            #Counts the number of bad choices. If more than 2, the rest will be good selections
             number = meal_choices.count("bad")
             if number >= 2:
                meal_choice = "good"
             else:
                meal_choices += meal_choice
            #Selects a random food from that selection type
             item = random.randint(0,8)
            #Checks if that selection has already been chosen. Meant to ensure that no food shows up twice on the same
            #screen shot
             if item not in used:
                 #The middle two photos are further down on the screen for visual purposes
                if len(used) == 1 or len(used)==2:
                     y = y + 50
                #Ensures that no image portions of the previous selection remains behind the new selection.
                pygame.draw.rect(self.window, WHITE, (x, y, 100, 100), 0)
                pygame.draw.rect(self.window, BLUE, (x, y + 100, 200, 50), 0)
                #Loads/Displays Food Items
                food = pygame.image.load(self.__food[meal_choice][item])
                food = pygame.transform.scale(food, (100, 100))
                #Rights the Name of the Selection
                name = myfont.render(self.__food[meal_choice][item][:-4], False, WHITE)
                names += [self.__food[meal_choice][item]]
                self.window.blit(name, (x, y+100))
                self.window.blit(food, (x, y))
                #Updates Screen
                pygame.display.update()
                #As soon as the middle dishes are displayed, the last photo moves further up the screen
                if len(used) == 1 or len(used) == 2:
                    y = y - 50
                #Ensures the food does not show up again on the screen
                used += [item]
                x += 250
        choice = None
        #Waits until a selection has been made by the user
        while choice == None:
            choice = self.detect_button(names)
            if choice in self.__food["bad"]:
                #Calls player class to lower health level
                self.__player.health_level = "bad", self
                #If the player's status is still alive, a sick photo will be displayed to show why their health was
                #lowered.
                if self.__crashed == False:
                    sick = pygame.image.load(self.__poisons[random.randint(0,4)])
                    self.window.blit(sick,(0,0))
                    pygame.display.update()
                    #Allows time to read the caption on the photo
                    time.sleep(1)
                    #Resets the background image
                    self.draw_original_background()
            #Calls player class to increase health level
            elif choice in self.__food["good"]:
                self.__player.health_level = "good",self
        #Updates health bar
        health = pygame.image.load(self.__health_levels[self.__player.health_level])
        health = pygame.transform.scale(health, (500, 75))
        self.window.blit(health, (500, 0))

    def detect_button(self, food_choices):
        """
        Implements pygame functionality to see if food is being clicked. The food
        correspond to the randomly selected food names.

        :param list food_choices: The list of names of selected food options.
        :return string name: name of food chosen, otherwise returns None
        """
        mouse = pygame.mouse.get_pos()
        for event in pygame.event.get():
        # food 1
            if 70 <= mouse[0] <= 170 and 350 <= mouse[1] <= 450 and event.type == pygame.MOUSEBUTTONDOWN:
                return food_choices[0]
        # food 2
            elif 320 <= mouse[0] <= 420 and 400 <= mouse[1] <= 500 and event.type == pygame.MOUSEBUTTONDOWN:
                return food_choices[1]
        # food 3
            elif 570 <= mouse[0] <= 670 and 400 <= mouse[1] <= 500 and event.type == pygame.MOUSEBUTTONDOWN:
                return food_choices[2]
        # food 4
            elif 820 <= mouse[0] <= 920 and 350 <= mouse[1] <= 450 and event.type == pygame.MOUSEBUTTONDOWN:
                return food_choices[3]
        #If selection has yet to be made, return None so that the loop will continue until one has been made
            else:
                return None
    def draw_original_background(self):
        #Color Codes
        BLACK = (0, 0, 0)
        WHITE = (255, 255, 255)
        pygame.font.init()
        WHITE = (255, 255, 255)
        DARKBLUE = (0, 0, 139)
        BLUE = (0, 0, 255)
        GRAY = (192, 192, 192)
        GREEN = (0, 255, 0)
        RED = (255, 0, 0)
        #Draws Bottom Part of Table
        pygame.draw.rect(self.window, DARKBLUE, (0, 750, 1000, 250), 0)
        #Displays Drone
        plane = pygame.image.load('drone1.jpg')
        plane = pygame.transform.scale(plane, (1000, 300))
        self.window.blit(plane, (0, 0))
        #Draws upper part of table
        pygame.draw.rect(self.window, BLUE, (0, 300, 1000, 450), 0)
        #Displays your meal friend
        mouse = pygame.image.load('mouse.png')
        mouse = pygame.transform.scale(mouse, (150, 125))
        self.window.blit(mouse, (700, 560))
        #Displays Plate
        plate = pygame.image.load('Plate2.png')
        plate = pygame.transform.scale(plate, (200, 170))
        self.window.blit(plate, (400, 570))
        #Draws the Title with Appropriate Font and Color
        myfont = pygame.font.SysFont('Broadway', 100)
        textsurface = myfont.render('Mitches Gives', False, WHITE)
        self.window.blit(textsurface, (130, 760))
        textsurface1 = myfont.render('You', False, WHITE)
        self.window.blit(textsurface1, (130, 890))
        myfont = pygame.font.SysFont('SWTxt', 100)
        text = myfont.render('Glitches', False, GREEN)
        self.window.blit(text, (370, 880))
        pygame.display.update()


if __name__ == "__main__":
    main()
