#!/usr/bin/env python3
"""
PEX 5- Code Template
Mitches Give You Glitches
CS 210, Introduction to Programming
"""

import tkinter as tk
from PIL import Image, ImageTk
import pygame
import os  # For os.path.basename.

__author__ = "Sarah Placke and Allison Raines"
__section__ = "T6/7"
__instructor__ = "Maj Sievers"
__date__ = "16 Nov 2017"
__documentation__ = """ Used templates from PEX3 and PEX4 """


def main():
    """
    Main program to run Mitches video game
    """
    pygame.init()
    pygame.display.set_mode((600,600))
    title = "Mitches Gives You Glictches"
    pygame.display.set_caption(title)
    clock = pygame.time.Clock()
    FPS = 30
    program = Mitches()
    while True:
        clock.tick(FPS)
        pygame.display.update()
        program.window.mainloop()

def end_game(player):
    """
    This method will be called after a player has finished the game.
    It shall update the screen and internal variables as needed.
    :param Player player: my player object
    """


class Player:
    """ A player in the Mitches game. """

    def __init__(self, name="Player"):
        """
        Initializes a new Player with a name, health and meals.
        :param str name: the player's name
        """
        self.__name = name
        self.__health_level = 100
        self.__meals = 0


    @property
    def name(self):
        """
        Provides read-access to the name property.
        :return: player's name
        :rtype: str
        """
        return self.__name
    @property
    def health_level(self):
        """
        Provides read-access to the health property.
        Key: 0 = dead, 1 = poor, 2 = average, 3 = good, 4 = excellent
        :return: player's health
        :rtype: int
               """
        return self.__health_level
    @health_level.setter
    def health_level(self, new_health):
        """
        Provides write-access to the health property.

        If a health is set that is longer that is less than 0 or greater than 4,
        print to the console to let the programmer know they are accessing this
        property in the incorrect way.
        :param int new_health: the new health rating
        """
        self.__health_level = new_health
    @property
    def meals(self):
        """
        Provides read-access to the meals property.
        :return: player's meals (out of 5 levels)
        :rtype: list
        """
        return self.__meals
    @meals.setter
    def meals(self, next_level):
        """
        Provides write-access to the meals property.

        If the meals property is already 5, there is no greater level.
        Print to the console to let the programmer know they are accessing
        this property in the incorrect way.
        :param bool next_level: whether to move on to the next meal.
        """
        if next_level == True:
            if self.__meals == 10:
                end_game(self)
            else:
                self.__meals += 1
        else:
            end_game(self)

    def up_level(self):
        """
        This method will be called after a player makes a food choice that raises
        their health to 4, excellent. They move onto the next meal/level.
        It shall update internal variables as needed.
        """


class Mitches:
    """ The Mitches game console. """

    def __init__(self):
        """
        Initializes a new Mitches game with dictionary of food names:files.
        """
        self.__food = {"breakfast":["waffles","bacon","breakfast burrito", "eggs", "biscuits","ham"],"lunch":[
            "corn bread", "fish", "green pasta","mac n cheese","fried chicken","rice"],
            "dinner":["steak","turkey","pasta", "soup","salad","brussels sprouts"]}

        # self.window = tk.Tk()
        # self.window.title("Mitches Gives You Glitches")
        # self.strength_canvas = None  # type: tk.Canvas
        #
        # self.background_frame = tk.Frame(self.window)

        self.update_background()


    @property
    def food(self):
        """
        Provides read-access to the food property.
        :return: game's dictionary of food
        :rtype: dict
        """
        return self.__food
    def update_background(self):
        """
        Prints the table as a background and randomly selects 6 food
        items from the dictionary. Prints the corresponding food images.
        Updates health meter and game information (meals left, game level).
        """
        # self.strength_canvas = tk.Canvas(self.window, bg="red", width=100, height=10)
        # self.strength_canvas.grid(row=1, column=0, sticky=tk.W + tk.E)
        # self.strength_canvas.create_line(0, 5, 5, 0, width=5, fill="white", tags="strength")

        bird = Image.open("bread.jpg")
        bird = ImageTk.PhotoImage(bird)

        panel = tk.Button(self.background_frame, image = bird)
        panel.grid(row = 1, column = 1)


        # panel.pack(side = "bottom", fill = "both", expand = "yes")
############################
    def create_buttons(self, food_choices):
        """
        Implements GUI functionality to display buttons. The buttons will
        correspond to the randomly selected food names.

        :param list food_choices: The list of names of selected food options.
        """

if __name__ == "__main__":
    main()
