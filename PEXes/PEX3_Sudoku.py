#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

import easygui  # For easygui.fileopenbox.
import os  # For os.path.basename.

__author__ = "Sarah Placke"  # Your name. Ex: John Doe
__section__ = "T6"  # Your section. Ex: M1
__instructor__ = "Maj Sievers"  # Your instructor. Ex: Lt Col Doe
__date__ = "12 Oct 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ None """  # Multiple lines OK with triple quotes

DATA_DIRECTORY = "../Data/Sudoku"


def main():
    """
    Main program to do Sudoku, so here's a Sudoku haiku:

    One through nine in place
    To open the matrix door
    Let logic guide you
    """
    filename = 0
    while filename != None:
        filename = easygui.fileopenbox(default="../Data/Sudoku/*.txt")
        matrix = open_sudoku(filename)
        print_sudoku(matrix)
        count = 0
        if is_solved(matrix):
            print("The puzzle IS valid and IS solved.")

        elif is_valid(matrix):
            print("The puzzle IS valid and is NOT solved.")

        else:
            print("The puzzle is NOT valid and is NOT solved.")


            # TODO 1b: Demonstrate the str_sudoku function with the given puzzles.
            # print( str_sudoku( PUZZLE ) )
            # print( str_sudoku( PUZZLE_SOLVED ) )

            # TODO 2b: Demonstrate the print_sudoku function with the given puzzles.
            # print_sudoku( PUZZLE )
            # print_sudoku( PUZZLE_SOLVED )

            # TODO 3b: Demonstrate the create_sudoku puzzle with the given data.
            # print_sudoku( create_sudoku(data) )
            # print_sudoku( create_sudoku( DATA_SOLVED ) )

            # TODO 4b: Demonstrate the open_sudoku function with the given files.
            # for test_filename in ["Sudoku_Blank.txt", "Sudoku00.txt", "Sudoku01.txt", "Sudoku02.txt", "Sudoku03.txt]:
            #     fullpath = os.path.join(DATA_DIRECTORY, test_filename)
            #     sudoku_data = open_sudoku(fullpath)
            #     print_sudoku(sudoku_data)

            # TODO 5b: Demonstrate the is_solved function with the given puzzles.
            # print( is_solved( PUZZLE ), is_solved( PUZZLE_SOLVED ) )

            # TODO 6b: Demonstrate the is_valid function with the given puzzles.
            # print( is_valid( PUZZLE ), is_valid( PUZZLE_SOLVED ), is_valid( PUZZLE_INVALID ) )

            # TODO 7: Implement the main program as described


# TODO 1a: Implement the str_sudoku function as described
def str_sudoku(puzzle):
    """
    Creates a string of puzzle values suitable for printing to a file.

    The string created by this function is formatted such that it could be
    passed to create_sudoku function and recreate the same puzzle.

    Note: This function DOES NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: A string suitable for printing to a file or passing to create_sudoku.
    :rtype: str
    """
    data = ''
    for row in range(0, 9):
        for number in range(0, 9):
            # turns the number into a str and adds a space for visual
            data += str(puzzle[row][number]) + ' '
        # creates next row for data
        data += '\n'

    return data


# TODO 2a: Implement the print_sudoku function as described
def print_sudoku(puzzle):
    """
    Prints the nested list structure in pretty rows and columns.

    For example, PUZZLE (bottom of this file) would print as follows:
    +===+===+===+===+===+===+===+===+===+
    # 1 | 8 |   # 6 |   | 9 #   | 5 | 7 #
    +---+---+---+---+---+---+---+---+---+
    # 5 |   |   #   |   |   #   |   | 3 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 2 #   | 8 |   # 4 |   |   #
    +===+===+===+===+===+===+===+===+===+
    # 7 |   |   # 8 | 4 | 5 #   |   | 1 #
    +---+---+---+---+---+---+---+---+---+
    #   |   | 3 # 2 |   | 1 # 9 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 2 |   |   # 9 | 6 | 3 #   |   | 5 #
    +===+===+===+===+===+===+===+===+===+
    #   |   | 1 #   | 5 |   # 8 |   |   #
    +---+---+---+---+---+---+---+---+---+
    # 8 |   |   #   |   |   #   |   | 4 #
    +---+---+---+---+---+---+---+---+---+
    # 9 | 4 |   # 3 |   | 8 #   | 7 | 2 #
    +===+===+===+===+===+===+===+===+===+

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: None
    """
    counter = 2
    row = 0
    # Variable used to signify when to move to next row
    yes = 0
    for line in range(0, 19):
        index = 0
        for item in range(0, 19):
            # prints the fancy outlining of data/ Lines separating data
            if line % 2 == 0:
                # every other column in even number rows
                if item % 2 == 0:
                    print("+", end='')

                # every other column in every third row
                elif item % 2 == 1 and line % 3 == 0:
                    print("===", end='')

                else:
                    print("---", end='')

            # prints aspects between the data
            elif item % 2 == 0:
                # prints hashtags in a fashion that defines boxes
                if item == 0 or item == 18 or item == 6 or item == 12:
                    print("#", end=' ')
                else:
                    print("|", end=' ')

            # prints data
            else:
                if puzzle[row][index] == 0:
                    print(' ', end=' ')
                else:
                    print(puzzle[row][index], end=' ')
                if index < 9:
                    # indicates which number in the row to print
                    index += 1
                # States that a data line has been printed
                yes = 1
        print('')
        # Ensures that index does not go out of range. Doesn't up row unless data line was printed
        if yes == 1 and row < 9:
            row += 1
        # Resets y so that row will not update unless data line was printed
        yes = 0


# TODO 3a: Implement the create_sudoku function as described
def create_sudoku(data):
    """
    Creates a 9x9 nested list of integers from a string with 81 separate values.

    :param str data: A string with 81 separate integer values, [0-9].
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    build = []
    matrix = []
    for item in data.split():
        # Ensures that only numbers get through
        if item.isdigit() == True:
            # Builds a normal (list[str])
            build += [item]

    for column in range(9):
        row = []
        for number in range(9):
            # Builds a row as list[int]
            row += [int(build[number])]
        # Adds list[int] to another list to create a list[list[int]] by rows of data
        matrix += [row]

        if column < 9:
            for delete in range(9):
                # Removes the integers just added to the matrix (Allows for length of build
                # to change thereby allowing the same nested loop to used the emtire time
                build.remove(build[0])

    return matrix


# TODO 4a: Implement the open_sudoku function as described
def open_sudoku(filename):
    """
    Opens the given file, parses the contents, and returns a Sudoku puzzle.

    This function prints to the console any comment lines in the file.

    :param str filename: The file name.
    :return: The Sudoku puzzle as a 9x9 nested list of integers.
    :rtype: list[list[int]]
    """
    with open(filename) as data_file:
        data = data_file.read()
    print(os.path.basename(data))
    yes = 0
    # Variable set so that the space after the hashtag will not be printed
    space = 0
    data_copy = ''
    # counts how many sentences are in the comments (messages after #)
    number = data.count(".")

    for line in data:
        for word in line:
            # letter represents every character in the data file
            for letter in word:
                # Will only print letters after hashtag
                if letter != '#' and yes < number and letter != '.' and space == 1:
                    print(letter, end='')

                elif letter == ".":
                    # One of the comments(hashtag sentences) have been written
                    yes += 1
                    # Enters a new line after sentence
                    print('.\v')
                    # Resets variable so that the space after the next hashtag is not printed
                    space = 0

                # All comments have been written
                elif yes == number:
                    # Creating a new list with only the number
                    data_copy += letter

                else:
                    if letter != '#' and letter != "\n":
                        # Resets variable so that the rest of the spaces are printed
                        space = 1

    return (create_sudoku(data_copy))


# TODO 5a: Implement the is_solved function as described
def is_solved(puzzle):
    """
    Determines if a Sudoku puzzle is valid and complete.

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid and complete; False otherwise.
    :rtype: bool
    """
    # Checks Rows
    for i in range(9):
        # list created to check if there are any repeated numbers 1-9 in the specified row
        marked = []

        for j in range(9):
            # if a number is repeated, it is not solved
            if puzzle[i][j] in marked:
                return False

            # Ensures no input is outside of the allowed range
            elif puzzle[i][j] < 1 or puzzle[i][j] > 9:
                return False

            else:
                # Adds integers to row list to be checked for repetition later
                marked += [puzzle[i][j]]

    # Checks Columns
    counter = 0
    while counter < 9:
        # Creates/Clears list to enter used elements
        othermarked = []
        for i in range(9):
            # Counter is used to ensure that the program is checking the same element in each row (checks column)
            if puzzle[i][counter] in othermarked and puzzle[i][counter] != 0:
                return False

            else:
                othermarked += [puzzle[i][counter]]
        counter += 1

    # Variable used to control which columns will be used
    insert = 0
    for counter in range(3):
        # creates/clears empty lists for each 3 boxes
        box_one = []
        box_two = []
        box_three = []
        for big_row in range(3):
            for box in range(9):
                # Does three boxes side by side per rotation
                if box < 3:
                    # stores each integer in a specific list
                    box_one += [puzzle[insert][box]]

                elif box > 2 and box < 6:
                    box_two += [puzzle[insert][box]]

                else:
                    box_three += [puzzle[insert][box]]

            # Added to have the program go down a row
            insert += 1

        # Goes through each element in the given lists
        for item in range(9):
            # if the integer is in the list more than once, it is a repeat, and the sudoku is not solved
            if box_one.count(box_one[item]) > 1:
                return False
            elif box_two.count(box_two[item]) > 1:
                return False
            elif box_three.count(box_three[item]) > 1:
                return False

    return True


# TODO 6a: Implement the is_valid function as described
def is_valid(puzzle):
    """
    Determines if a Sudoku puzzle is valid, but not necessarily complete.

    Note: This function MUST NOT modify the puzzle.

    :param list[list[int]] puzzle: The Sudoku puzzle as a 9x9 nested list of integers.
    :return: True if the puzzle is valid; False otherwise.
    :rtype: bool
    """
    # Checks Rows
    for i in range(9):
        # Creates/clears list to fill in with used numbers in each row
        marked = []
        for j in range(9):
            # Not equal to zero, because 0s can occur multiple times
            if puzzle[i][j] in marked and puzzle[i][j] != 0:
                return False
            # Range is set for 0-9 because valid can contain 0's
            elif puzzle[i][j] < 0 or puzzle[i][j] > 9:
                return False
            else:
                marked += [puzzle[i][j]]

    # Checks Columns
    # Variable used to ensure that the same element is being used to check columns
    counter = 0
    while counter < 9:
        # Creates/clears list to fill with used numbers in each column
        othermarked = []
        # Checks for repeated numbers outside of 0
        for i in range(9):
            if puzzle[i][counter] in othermarked and puzzle[i][counter] != 0:
                return False
            # Adds integer to list to check for duplications
            else:
                othermarked += [puzzle[i][counter]]
        counter += 1

    # Variable used to control which row is being evaluated
    insert = 0
    for counter in range(3):
        # Create/Clears lists to checks for duplications in boxes
        box_one = []
        box_two = []
        box_three = []
        for i in range(3):
            for j in range(9):
                # Indicates left box
                if j < 3:
                    box_one += [puzzle[insert][j]]
                # Indicates middle box
                elif j > 2 and j < 6:
                    box_two += [puzzle[insert][j]]
                # Indicates right box
                else:
                    box_three += [puzzle[insert][j]]
            insert += 1

        for i in range(9):
            # There are allowed multiple blanks, so only numbers 1-9 will be checked for repetition
            if box_one.count(box_one[i]) > 1 and box_one[i] != 0:
                return False

            elif box_two.count(box_two[i]) > 1 and box_two[i] != 0:
                return False

            elif box_three.count(box_three[i]) > 1 and box_three[i] != 0:
                return False

    return True


# TODO 8: Implement the solve_sudoku function as discussed in class
def solve_sudoku(puzzle, cell = 0, value = 1):
    """
    Recursive function to solve a Sudoku puzzle with brute force.

    Note: This function DOES modify the puzzle!!!

    :param list[list[int]] puzzle:  The 9x9 nested list of integers.
    :return: None
    """
    if (is_valid(puzzle)) and (not is_solved(puzzle)) and cell<81 and value <=9:
        row = cell//9
        col = cell % 9
        if puzzle[row][col] != 0:
            solve_sudoku(puzzle,cell+1,1)
        else:
            puzzle[row][col] = value
            solve_sudoku(puzzle,cell+1,1)
            if not is_solved(puzzle):
                puzzle[row][col] = 0
                solve_sudoku(puzzle,cell,value+1)


"""
The following puzzles in list and string form are provided to help you
test and demonstrate your code.
"""

PUZZLE = [[1, 8, 0, 6, 0, 9, 0, 5, 7],
          [5, 0, 0, 0, 0, 0, 0, 0, 3],
          [0, 0, 2, 0, 8, 0, 4, 0, 0],
          [7, 0, 0, 8, 4, 5, 0, 0, 1],
          [0, 0, 3, 2, 0, 1, 9, 0, 0],
          [2, 0, 0, 9, 6, 3, 0, 0, 5],
          [0, 0, 1, 0, 5, 0, 8, 0, 0],
          [8, 0, 0, 0, 0, 0, 0, 0, 4],
          [9, 4, 0, 3, 0, 8, 0, 7, 2]]

PUZZLE_SOLVED = [[1, 8, 4, 6, 3, 9, 2, 5, 7],
                 [5, 9, 7, 1, 2, 4, 6, 8, 3],
                 [6, 3, 2, 5, 8, 7, 4, 1, 9],
                 [7, 6, 9, 8, 4, 5, 3, 2, 1],
                 [4, 5, 3, 2, 7, 1, 9, 6, 8],
                 [2, 1, 8, 9, 6, 3, 7, 4, 5],
                 [3, 7, 1, 4, 5, 2, 8, 9, 6],
                 [8, 2, 5, 7, 9, 6, 1, 3, 4],
                 [9, 4, 6, 3, 1, 8, 5, 7, 2]]

PUZZLE_INVALID = [[1, 8, 0, 6, 3, 9, 0, 5, 7],
                  [5, 0, 0, 0, 0, 0, 0, 0, 3],
                  [0, 0, 2, 0, 8, 0, 4, 0, 0],
                  [7, 0, 0, 8, 4, 5, 0, 0, 1],
                  [0, 0, 3, 2, 3, 1, 9, 0, 0],
                  [2, 0, 0, 9, 6, 3, 0, 0, 5],
                  [0, 0, 1, 0, 5, 0, 8, 0, 0],
                  [8, 0, 0, 0, 0, 0, 0, 0, 4],
                  [9, 4, 0, 3, 0, 8, 0, 7, 2]]

DATA = """1 8 0 6 0 9 0 5 7
          5 0 0 0 0 0 0 0 3
          0 0 2 0 8 0 4 0 0
          7 0 0 8 4 5 0 0 1
          0 0 3 2 0 1 9 0 0
          2 0 0 9 6 3 0 0 5
          0 0 1 0 5 0 8 0 0
          8 0 0 0 0 0 0 0 4
          9 4 0 3 0 8 0 7 2"""

DATA_SOLVED = """1 8 4 6 3 9 2 5 7
                 5 9 7 1 2 4 6 8 3
                 6 3 2 5 8 7 4 1 9
                 7 6 9 8 4 5 3 2 1
                 4 5 3 2 7 1 9 6 8
                 2 1 8 9 6 3 7 4 5
                 3 7 1 4 5 2 8 9 6
                 8 2 5 7 9 6 1 3 4
                 9 4 6 3 1 8 5 7 2"""

# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# d15606d9770cfd538b6d7c9c66cd23faa4bcb65a84818606563688257a8582d3 pfnenucynpxr
