#!/usr/bin/env python3
"""
Pex 4: Tic Tac Toe Game (Utilizing Classes)
CS 210, Introduction to Programming
"""
import easygui

__author__ = "Sarah Placke"  # Your name. Ex: John Doe
__section__ = "T6"  # Your section. Ex: M1
__instructor__ = "Maj Sievers"  # Your instructor. Ex: Lt Col Doe
__date__ = "13 Nov 2017"  # Today's date. Ex: 25 Dec 2017
__documentation__ = """ None """  # Multiple lines OK with triple quotes


def main():
    turn = 0
    # Creates Player Data
    name = input("What would you like your name to be?")
    marker = input("What would you like your marker to be?")
    player1 = Player(name, marker)
    name = input("What would you like your name to be?")
    marker = input("What would you like your marker to be?")
    player2 = Player(name, marker)
    # Continuously checks if a certain player has one twice or if three games have been played
    # If so, no more games will be played, and a winner is determined.
    while player1.wins < 2 and player2.wins < 2:
        # Alternates who gets to move first
        if turn % 2 == 0:
            tic = TicTacToe(player1, player2)
        else:
            tic = TicTacToe(player2, player1)
        # Updates board with player's moves and prints board after every turn
        while tic.turns_played < 10 and tic.winner == None:
            print(board_string(tic.board))
            TicTacToe.play_move(tic, int(input("Row:")), int(input("Column:")))
        turn += 1


def board_string(board):
    """Sets up board to print in proper/readable format
    :param TicTacToe board: the board to make readable
    :return: readable board
    :rtype str"""
    counter = 0
    new = ''
    # Separates given nested loop into single characters
    for object in board:
        for item in object:
            # Prints the first two characters in each row with a space after
            if counter != 2:
                new += str(item) + ' '
                counter += 1
            # Prints the third character in each row and then starts a new line
            else:
                new += str(item) + '\n'
                counter = 0
    return new


class TicTacToe:
    """ A Tic Tac Toe game model """

    def __init__(self, player1, player2):
        """
        Initializes a new TicTacToe game.
        :param Player player1: the first player to take a turn
        :param Player player2: the second player to take a turn
        """
        self.__current_player = player1
        self.__players = player1, player2
        self.__winner = None
        self.__loser = None
        self.__turns_played = 0
        self.__board = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

    def __iter__(self):
        return iter(self.__board)

    @property
    def current_player(self):
        """
        provides read-access to the the current player property.
            :return: current player
            :rtype: str
                                """
        return self.__current_player

    @property
    def players(self):
        """
        provides read-access to the the players property.
            :return: players
            :rtype: tuple
                                """
        return self.__players

    @property
    def winner(self):
        """
        provides read-access to the winner of the game property.
            :return: winner of the game
            :rtype: str
                                """
        return self.__winner

    @property
    def loser(self):
        """
        provides read-access to the loser of the game property.
            :return: loser of the game
            :rtype: str
                        """
        return self.__loser

    @property
    def turns_played(self):
        """
        provides read-access to the number of turns played property.
            :return: player's number of turns
            :rtype: int
                        """
        return self.__turns_played

    @property
    def board(self):
        """
         provides read-access to the board
            :return: updated board
            :rtype: TicTacToe
                        """
        return self.__board

    def play_move(self, row, column):
        """Updates Board and Checks for a Winner/ Updates accordingly
                :param int row: the row of the desired coordinate
                :param int column: the column of the desired coordinate
                :return: board
                :rtype: TicTacToe"""
        # Alters given coordinates and updates board
        self.__board[row - 1][column - 1] = self.__current_player.marker
        self.__turns_played += 1
        # Switches who the current player is/switches turns
        if self.__current_player == self.__players[0]:
            self.__current_player = self.__players[1]
        else:
            self.__current_player = self.__players[0]

        # Checks for a winner
        winner = 0
        for i in range(3):
            row = []
            column = []
            # Splits board into two lists, one of a row and one of the corresponding column
            for j in range(3):
                row += [self.__board[i][j]]
                column += [self.__board[j][i]]
            # Checks row to see if it consists of three of the same character
            if row[0] == row[1] == row[2]:
                self.winner = row[0]
                winner = 1
                break
            # Checks column for three of the same character
            elif column[0] == column[1] == column[2]:
                self.winner = column[0]
                winner = 1
                break
        # Checks Top Right to Bottom Left Diagonal for three of the same character
        if self.__board[0][2] == self.__board[1][1] == self.__board[2][0]:
            self.winner = self.__board[1][1]
            winner = 1
        # Checks Top Left to Bottom Right Diagonal for three of the same character
        elif self.__board[0][0] == self.__board[1][1] == self.__board[2][2]:
            self.winner = self.__board[0][0]
            winner = 1
        # If there is no winner after 9 turns, the game ends and each player gets a tie
        if winner == 0 and self.__turns_played == 9:
            self.__players[0].record_tie()
            self.__players[1].record_tie()

    def player_at(self, row, column):
        """Determines if a given coordinate is taken and who has taken it
        :param int row: the row of the desired coordinate
        :param int column: the column of the desired coordinate
        :return: player who has taken the spot or None if not taken
        :rtype: TicTacToe"""
        # Alters given coordinates to match nested loop
        result = self.__board[row - 1][column - 1]
        if result == self.__players[0].marker:
            return self.__players[0]
        elif result == self.__players[1].marker:
            return self.__players[1]
        # If not a player character, no one has put their piece there
        else:
            return None

    @winner.setter
    def winner(self, marker):
        """
                Establishes a winner
                :param str marker: the character of the winner
                """
        if marker == self.__players[0].marker:
            self.__winner = self.__players[0]
            self.__players[0].record_win()
            self.__loser = self.__players[1]
            self.__players[1].record_loss()

        else:
            self.__winner = self.__players[1]
            self.__players[1].record_win()
            self.__loser = self.__players[0]
            self.__players[0].record_loss()


class Player:
    """ A player in the Tic Tac Toe game. """

    def __init__(self, name, marker):
        """
        Initializes a new Player with a name and a marker.
        :param str name: the player's name
        :param str marker: the marker to use on the board
        """
        # Ensures a visible character is set as the marker
        if marker == '' or marker == None:
            marker = "!"
        # Ensures that only a single character is set as the marker
        else:
            marker = marker[0]
        self.__name = name
        self.__marker = marker
        self.__wins = 0
        self.__losses = 0
        self.__ties = 0
        self.__games_played = 0

    def __str__(self):
        """ Return a string representation of the player. """
        return ("{}: {}, {} wins, {} losses, {} ties".format(self.__name, self.__marker, self.__wins, self.__losses,
                                                             self.__ties))

    @property
    def name(self):
        """
                provides read-access to the name property.
                :return: player's name
                :rtype: str
                """
        return self.__name

    @property
    def marker(self):
        """
                provides read-access to the marker property.
                :return: number of wins
                :rtype: int
                """
        return self.__marker

    @property
    def wins(self):
        """
                provides read-access to the wins property.
                :return: number of wins
                :rtype: int
                """
        return self.__wins

    @property
    def losses(self):
        """
                provides read-access to the losses property.
                :return: number of losses
                :rtype: int
                """
        return self.__losses

    @property
    def ties(self):
        """
                provides read-access to the ties property.
                :return: number of ties
                :rtype: int
                """
        return self.__ties

    @property
    def games_played(self):
        """
                provides read-access to the games played property.
                :return: number of games played
                :rtype: int
                """
        return self.__games_played

    def record_win(self):
        """
        Updates the record of wins and games played for the player
        :return: number of wins, number of games played
        :rtype: int, int
        """
        self.__wins += 1
        self.__games_played += 1

    def record_loss(self):
        """
        Updates the record of losses and games played for the player
        :return: number of losses, number of games played
        :rtype: int, int
        """
        self.__losses += 1
        self.__games_played += 1

    def record_tie(self):
        """
        Updates the record of ties and games played for the player
        :return: number of ties, number of games played
        :rtype: int, int
        """

        self.__ties += 1
        self.__games_played += 1


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

if __name__ == "__main__":
    print(__doc__.strip())
    print("Author:", (__author__ if __author__ else "\033[91mBLANK (You must fill in the __author__ metadata)\033[0m"))
    print("Section:",
          (__section__ if __section__ else "\033[91mBLANK (You must fill in the __section__ metadata)\033[0m"))
    print("Instructor:",
          (__instructor__ if __instructor__ else "\033[91mBLANK (You must fill in the __instructor__ metadata)\033[0m"))
    print("Date:", (__date__ if __date__ else "\033[91mBLANK (You must fill in the __date__ metadata)\033[0m"))
    print("Documentation:", (
        __documentation__ if __documentation__.strip() else
        "\033[91mBLANK (You must fill in the __documentation__ metadata)\033[0m"))
    _ = b'CmltcG9ydCBnZXRwYXNzLCBoYXNobGliLCBjb2RlY3MsIHN0cmluZyBhcyBfX1MKdSA9IGdldHBhc3MuZ2V0dXNlcigpCmggPSBoYXN' + \
        b'obGliLnNoYTI1Nih1LmVuY29kZSgpKS5oZXhkaWdlc3QoKQpyID0gY29kZWNzLmVuY29kZSh1Lmxvd2VyKCkudHJhbnNsYXRlKHtvcm' + \
        b'Qoayk6IE5vbmUgZm9yIGsgaW4gX19TLmRpZ2l0c30pLnJlcGxhY2UoJy4nLCcnKSwgJ3JvdF8xMycpCndpdGggb3BlbihfX2ZpbGVfX' + \
        b'ywgInIiKSBhcyBmOgogICAgaWYgaCBub3QgaW4gZi5yZWFkKCk6CiAgICAgICAgd2l0aCBvcGVuKF9fZmlsZV9fLCAiYSIpIGFzIGY6' + \
        b'CiAgICAgICAgICAgIHByaW50KCIjIiwgaCwgciwgZmlsZT1mKQo='
    # noinspection PyBroadException
    try:
        import base64

        eval(compile(base64.b64decode(_), '<string>', "exec"))
    except:
        pass
    finally:
        main()
# d15606d9770cfd538b6d7c9c66cd23faa4bcb65a84818606563688257a8582d3 pfnenucynpxr
