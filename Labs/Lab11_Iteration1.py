#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

import easygui
import random

__author__ = "Sarah Placke"
__instructor__ = "Maj Sievers"
__date__ = "12 Sep 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 11: Iteration from our online textbook
-	Watch the embedded video on the while statement
Lesson Objectives
-	Reinforce functions, parameters, return values, and selection statements
-	Reinforce definite iteration with the for loop
-	Introduce indefinite iteration with the while loop


"""


def main():
    """Main program to test solutions for each exercise."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    #exercise0()
    #exercise1()
    #exercise2()
    #exercise3()
    exercise4()
    #exercise5()


def exercise0():
    """Example code."""
    print_exercise_name()

    # Count by 3s with for loop
    for i in range(3, 20, 3):
        print(i, " ", end="")
    print()

    # Equivalent count by 3s with while loop
    n = 3
    while n < 20:
        print(n, " ", end="")
        n = n + 3
    print()


def exercise1():
    """
    Interact with the user and test the sum_odds and sum_evens functions (from Lab08).

    a.  In the space "TODO 1a", add four more tests to those provided that show
        that your function will work as expected.
        NOTE: Run the exercise1() function, which calls the existing sum_odds and
        sum_evens functions (known to work properly) to generate good test numbers.

    b.	In the space "TODO 1b", re-write the indicated code using a while loop
        instead of a for loop.

    c.	In the space "TODO 1c", re-write the indicated code using a while loop
        instead of a for loop.
    """
    print_exercise_name()

    # Test your function
    if sum_odds(10) != 25:
        print("Incorrect: 10 odds")
    if sum_evens(10) != 30:
        print("Incorrect: 10 evens")
    # TODO 1a: Add four more examples to the two above that test your function
    if sum_odds(5) != 9:
        print("Incorrect: 5 odds")
    if sum_evens(16)!=72:
        print("Incorrect: 16 evens")
    if sum_odds(3)!= 4:
        print("Incorrect: 3 odds")
    if sum_evens(6) !=12:
        print("Incorrect: 6 evens")
    ###This is not going to this point###
    # You _DO_NOT_ need to modify this code.
    n = easygui.integerbox("Exercise 1\nEnter n:", "Input", lowerbound=0, upperbound=2 ** 31)
    odd = sum_odds(n)
    even = sum_evens(n)
    easygui.msgbox("n = {}\nsum of odds = {}\nsum of evens = {}".format(n, odd, even))


def sum_odds(n):
    """Calculate and return the sum of the odd numbers between 1 and n, inclusive.

    :param int n: The upper bound of the series to sum.
    :return: The sum of the odd numbers between 1 and n, inclusive.
    :rtype: int
    """
    # TODO 1b: Re-write the code below using a while loop instead of a for loop.
    result = 0
    x = 1
    while x>0 and x<n+1:
        if x%2==1:
            result += x
        x = x+1
    return result


def sum_evens(n):
    """Calculate and return the sum of the even numbers between 1 and n, inclusive.

    :param int n: The upper bound of the series to sum.
    :return: The sum of the even numbers between 1 and n, inclusive.
    :rtype: int
    """
    # TODO 1c: Re-write the code below using a while loop instead of a for loop.
    result = 0
    x = 1
    while x>0 and x<n+1:
        if x%2==0:
            result += x
        x = x+1
    return result


def exercise2():
    """
    Interact with the user and test the summation function (from Lab08).

    a.  In the space "TODO 2a", add three more tests to those provided that show
        that your function will work as expected.
        NOTE: Run the exercise2() function, which calls the existing summation
        function (known to work properly) to generate good test numbers.

    b.	In the space "TODO 2b", re-write the indicated code using a while loop
        instead of a for loop.

    """
    print_exercise_name()

    # Test your function
    if summation(10, 1) != 55:
        print("Incorrect: n=10, i^1")
    if summation(10, 2) != 385:
        print("Incorrect: n=10, i^2")
    if summation(10, 3) != 3025:
        print("Incorrect: n=10, i^3")
    # TODO 2a: Add three more examples to the three above that test your function
    if summation(11, 1) != 66:
        print("Incorrect: n=11, i^1")
    if summation(11, 2) != 506:
        print("Incorrect: n=11, i^2")
    if summation(11, 3) != 4356:
        print("Incorrect: n=11, i^3")
    # You _DO_NOT_ need to modify this code
    n = easygui.integerbox("Exercise 2\nEnter n:", "Input", lowerbound=0, upperbound=2 ** 31)

    s = summation(n, 1)
    f = n * (n + 1) // 2
    easygui.msgbox("n = {}\nsummation(n, 1) = {}\nformula result = {}".format(n, s, f))

    s = summation(n, 2)
    f = n * (n + 1) * (2 * n + 1) // 6
    easygui.msgbox("n = {}\nsummation(n, 2) = {}\nformula result = {}".format(n, s, f))

    s = summation(n, 3)
    f = (n * (n + 1) // 2) ** 2
    easygui.msgbox("n = {}\nsummation(n, 3) = {}\nformula result = {}".format(n, s, f))


def summation(n, exponent):
    """Calculation and return the summation of the series 1**exponent + 2**exponent + ... + n**exponent.

    :param int n: The upper bound of the series to sum.
    :param int exponent: The exponent for each term in the series.
    :return: The summation of the series.
    :rtype: int
    """
    # TODO 2b: Re-write the code below using a while loop instead of a for loop.
    result = 0
    x=1
    while x>0 and x<n+1:
        result += x ** exponent
        x=x+1
    return result


def exercise3():
    """
    Interact with the user and test the count_multiples function (from Lab08).

    a.  In the space "TODO 3a", add three more tests to those provided that show
        that your function will work as expected.
        NOTE: Run the exercise3() function, which calls the existing count_multiples
        function (known to work properly) to generate good test numbers.

    b.	In the space "TODO 3b", re-write the indicated code using a while loop
        instead of a for loop.
    """
    print_exercise_name()

    # Test your function
    if count_multiples(10, 20, 3) != 3:
        print("Incorrect: (10, 20, 3)")
    if count_multiples(1947, 2017, 4) != 18:
        print("Incorrect: (1947, 2017, 4)")
    # TODO 3a: Add three more examples to the three above that test your function
    if count_multiples(11,21,3) != 4:
        print("Incorrect:(11,21,3)")
    if count_multiples(5,10,3) != 2:
        print("Incorrect: (5,10,3)")
    if count_multiples (10,20,2) != 6:
        print("Incorrect:(10,20,2)")
    # You _DO_NOT_ need to modify this code for Lab 10.
    start = easygui.integerbox("Enter start value:", "Input", lowerbound=-2 ** 31, upperbound=2 ** 31)
    stop = easygui.integerbox("Enter stop value:", "Input", lowerbound=-2 ** 31, upperbound=2 ** 31)
    step = easygui.integerbox("Enter divisor value:", "Input", lowerbound=-2 ** 31, upperbound=2 ** 31)
    easygui.msgbox("There are {} multiples of {} in the range [{},{}].".format(
        count_multiples(start, stop, step), step, start, stop), "Result")

"""wont run if statements or easygui"""
def count_multiples(start, stop, divisor):
    """Count and return the number of values between start and stop, inclusive, evenly divisible by divisor.

    :param int start: The start value for the range, inclusive.
    :param int stop: The stop value for the range, inclusive.
    :param int divisor: The divisor to be counted.
    :return: The number of values in the range [start, stop] evenly divisible by divisor.
    :rtype: int
    """
    # TODO 3b: Re-write the code below using a while loop instead of a for loop.
    count = 0
    x = start
    while x>(start-1) and x<(stop+1):
        if x % divisor == 0:
            count += 1
        x=x+1
    return count


def exercise4():
    """
    Interact with the user and test the count_sevens function.

    a.	In the space "TODO 4a", write a function named roll_sevens that
        simulates repeatedly rolling two six-sided dice. The function receives
        an integer value as a parameter specifying the number of times the function
        should roll the value 7. When the value 7 has been rolled the specified number
        of times, the function should return the total number of times the dice were rolled.

        Also, to avoid the possibility (however remote) of an infinite loop, the function
        should also stop rolling the dice if the total number of rolls ever exceeds twice
        the EXPECTED number of rolls to reach the requested number of 7s. If this happens,
        it still returns the total number of rolls as normal (but the results shown by
        the exercise4 function will be invalid as the requested number of 7s was not reached.

    Hint: How many rolls would you expect it to take to roll a single 7?
    How many to roll ten 7s? One-hundred?

    Background: http://www.mastersetter.com/dice/rolling/probability/

    b.	In the space "TODO 4b", write code that uses the easygui.integerbox
        to obtain an integer value from the user for the desired number of 7s.
        Use the value entered as the parameter to the roll_sevens function and
        then display the results in an easygui.msgbox, including all of the
        information in the result dialog as shown below.

    Hint: The roll_sevens function does not contain any calls to the easygui module;
	all user interaction is done in the exercise4 function.

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |          How many 7s:                 |
            |  [ 1000                          ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +------------------------------------------+
            |                              [_] [ ] [X] |
            +------------------------------------------+
            |  1000 out of 5995 rolls (16.68%) were 7. |
            |             +------+                     |
            |             |  OK  |                     |
            |             +------+                     |
            +------------------------------------------+
    """
    print_exercise_name()

    # TODO 4b: Write code to use the roll_sevens function as described
    import easygui
    n = easygui.integerbox("Please input the number of times you wish for lucky number 7","Roll of the Dice",None)
    x,y,percent=roll_sevens(n)
    msg = ("{} out of {} rolls {}% were 7").format(x,y,percent)
    easygui.msgbox(msg,"Roll of the Dice",'OK')


# TODO 4a: In the space below, write the roll_sevens function as described
def roll_sevens(number):
    x=0
    y=0
    while x != number or x>number*100:
        n1 = random.randint(1,6)
        n2 = random.randint(1,6)
        total = n1 + n2
        if total == 7:
            x = x + 1
            print(x)

        y=y+1
        print(y)

    percent = round((x/y)*100,2)
    return x,y,percent
###Won't return x,y, or percent

def exercise5():
    """
    Score Keeper – Games such as racquetball, table tennis, and volleyball involve
    two players (or teams) earning points in a race to the winning score. However,
    these games include the caveat that the winner must win by at least two points.
    For example, if the winning score in a racquetball game is 15 and the players
    are tied at 14, the next point does not win the game. Instead, the game continues
    until one of the players has earned two points more than the opponent.

    a.	In the space "TODO 5a", write a function named scorekeeper that uses an
        easygui.buttonbox to keep score for a game such as racquetball or volleyball,
        as described above. The function receives three parameters: a string for the
        first player's name, a string for the second player's name, and an integer for
        the winning score of the game being played. The function returns a string
        indicating the winning player's name.

    b.	In the space "TODO 5b", write code that uses the easygui.enterbox (twice)
        to obtain the two player names and an easygui.integerbox to obtain an integer
        value for the winning score of the game. Use the values entered as the parameters
        to the scorekeeper function and then display the results in an easygui.msgbox.
        A series of interactions with the program is shown below.


            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |         Enter first player name:      |
            |  [ Scooby                        ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |         Enter second player name:     |
            |  [ Shaggy                        ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |      Enter required winning score:    |
            |  [ 3                             ]    |
            |        +------+       +--------+      |
            |        |  OK  |       | Cancel |      |
            |        +------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |      Scooby: 0                        |
            |        vs.                            |
            |       Shaggy: 0                       |
            |      Who wins the current point?      |
            |      +--------+       +--------+      |
            |      | Scooby |       | Shaggy |      |
            |      +--------+       +--------+      |
            +---------------------------------------+

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |      Scooby: 1                        |
            |        vs.                            |
            |       Shaggy: 0                       |
            |      Who wins the current point?      |
            |      +--------+       +--------+      |
            |      | Scooby |       | Shaggy |      |
            |      +--------+       +--------+      |
            +---------------------------------------+

            ... and so forth until one of them wins (by two) ...

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |       Scooby Wins!                    |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+


    """
    print_exercise_name()
    # TODO 5b: Write code to use the scorekeeper function as described
    import easygui
    first = easygui.enterbox("What is the first player's name?","Game Data",None)
    second = easygui.enterbox("What is the second player's name?","Game Data",None)
    score = easygui.enterbox("What is the targeted winning score?","Game Data",None)
    scorekeeper(first,second,score)

# TODO 5a: In the space below, write the scorekeeper function as described
def scorekeeper(first,second,score):
    game = 0
    x=0
    y=0
    data = (first,second)
    while game==0:
        msg = ("{}:{}\n{}:{}\nWho one the point?".format(first, x, second, y))
        n=easygui.boolbox(msg,"Game Data",choices=(list(data)))
        if n == True:
            x = x+1
        elif n == False:
            y = y+1
        if int(score)<=x or int(score)<=y:
            if x>=y+2 or y>=x+2:
                game=1
                if y+2<=x:
                    winner = first
                else:
                    winner = second
                easygui.msgbox(('The winner is {}!').format(winner), "Game Data", 'OK')


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.

# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
