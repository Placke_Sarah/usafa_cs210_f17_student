#!/usr/bin/env python3
"""
PUT YOUR DESCRIPTION OF THIS FILE HERE
CS 210, Introduction to Programming
"""

__author__ = "Sarah Placke"
__instructor__ = "Maj Sievers"
__date__ = "31 Aug 2017"
__documentation__ = """None"""  # No USAFA documentation statement required for Labs

"""
Preparation
-	Read Lesson 8: Functions from our online textbook
-	Watch the embedded video on the Accumulator Pattern

Lesson Objectives
-	Reinforce functions, parameters, and return values
-	Introduce Program Decomposition and the Accumulator Pattern


"""

# Import specific functions from the math library, just to demonstrate how to do it.
from math import atan2, sqrt, degrees
import easygui
import turtle
import math

# Define several useful constants to be used by the Turtle graphics.
WIDTH = 640  # A smaller window for this problem.
HEIGHT = WIDTH  # A square window for this problem.
MARGIN = 32  # Somewhat arbitrary value, but it looks nice.
FONT_SIZE = 16  # Somewhat arbitrary value, but it looks nice.
DRAW_FAST = True  # Set to True for fast, non-animated turtle movement.


def main():
    """Main program to test solutions for each problem."""
    # Print the docstring at the top of the file so your instructor can see your name.
    print(__author__, __doc__)

    # Call each individual exercise; comment/un-comment these lines as you work.
    #exercise0()
    #exercise1()
    #exercise2()
    exercise3()


def exercise0():
    """Example code."""
    print_exercise_name()

    your_name = easygui.enterbox("What is your name?")  # For strings
    your_age = easygui.integerbox("What is your age?", "Age", 18, 1, 99)  # For integers, also with min/max
    prompt = "Welcome, {}.\nAt {} years old, you must feel old.".format(your_name, your_age)
    easygui.msgbox(prompt)


"""
Before You Begin

Exercise 00.

The first few exercises in this lab require writing functions
to perform calculations using the Accumulator Pattern.
Begin your work by using pencil and paper to calculate a
few simple results. It is always useful to have a few known
results when writing such functions.

First, calculate the sum of the odd numbers between 1 and 20,
inclusive. Next, calculate the sum of the even numbers between
1 and 20, inclusive. You will use these values in exercise 1.

The formulas for calculating the result of the summations below
have been proven correct by mathematicians.

    Sum of i:

     n                                         n(n+1)
     Σ  i = 1 + 2 + 3 + 4 + ... + (n-1) + n =  -------
    n=1                                           2

    Sum of i squared:

     n   2   2   2   2   2             2   2     n(n+1)(2n+1)
     Σ  i = 1 + 2 + 3 + 4 + ... + (n-1) + n   =  ------------
    n=1                                               6

    Sum of i cubed:
                                                       2              2
     n   3   3   3   3   3             3   3    (  n  )     ( n(n+1) )
     Σ  i = 1 + 2 + 3 + 4 + ... + (n-1) + n   = (  Σ  )  =  ( ------ )
    n=1                                         ( n=1 )     (    2   )


Calculate the result of each summation with n = 5. Do this by
writing out the summation and also calculating the result with
the formula. For example, the first summation would be
	1 + 2 + 3 + 4 + 5 = 15
and the result from the formula would be
	5 * ( 5 + 1 ) / 2 = 5 * 6 / 2 = 30 / 2 = 15

Do these calculation by hand for the second and third
summations above; you will use these values in exercise 2.
"""


def exercise1():

    """
    Odd and Even Sums exercise.

    a.	In the space "TODO 1a", write a function named sum_odds that receives
        an integer value as a parameter. The function should use the
        accumulator pattern to calculate and return the sum of the odd
        numbers between 1 and the given value, inclusive.

    b.	In the space "TODO 1b", write a function named sum_evens that receives
        an integer value as a parameter. The function should use the
        accumulator pattern to calculate and return the sum of the even
        numbers between 1 and the given value, inclusive.

    c.	In the space "TODO 1c", write code that uses the easygui.integerbox to
        obtain an integer value from the user. Use the value entered as the
        parameter to both the sum_odds and sum_evens functions and then display
        the results in an easygui.msgbox as shown:

            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |          n = 20                       |
            |          sum of odds = 100            |
            |          sum of evens = 110           |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+
    """
    print_exercise_name()

    # TODO 1c: Write code to use the sum_odds and sum_evens functions
     # Remove the pass statement (and this comment) when writing your own code.

    x = (int(easygui.integerbox("What is your range?")))
    ODDS = int(sum_odds(x))
    EVENS = int(sum_evens(x))
    easygui.msgbox("n = {:d}\nsum of odds = {:d}\nsum of evens = {:d}".format(x,ODDS,EVENS))

# TODO 1a: In the space below, write the sum_odds function
def sum_odds(x):
    result = 0
    for i in range(x+1):
        if i % 2 == 1:
            result += i
    return result

# TODO 1b: In the space below, write the sum_evens function
def sum_evens(x):
    result = 0
    for i in range(x+1):
        if i%2==0:
            result += i

    return result


def exercise2():
    """
    Write a function that sums up a series of integers raised to a power, and compare
    your result to the "shortcut" formula given earlier in this file.

    a.	In the space "TODO 2a", write a function named summation that receives
        two integer parameters, one specifying the value of n for the summation and
        the other specifying the exponent for the summation (the only difference between
        the summations is the exponent). The function should use the accumulator pattern
        (not the formula) to calculate and return the result of the summation.

    b.	In the space "TODO 2b", write code that uses the easygui.integerbox to obtain
        an integer value from the user to be used as the n parameter to your summation
        function. With this value, display three successive easygui.msgbox dialogs. An
        example of the first with exponent = 1 is shown below.

    For example, for the first series above with exponent 1 (no exponent, essentially)
    you might write code like this:

        n = easygui.integerbox("Exercise 2\nEnter n:", "Input", lowerbound=0, upperbound=2 ** 31)
        s = summation(n, 1)
        f = n * (n+1) // 2
        easygui.msgbox( "n = {}, summation( n, 1 ) = {}, formula result = {}".format( n, s, f ) )


            +---------------------------------------+
            |                           [_] [ ] [X] |
            +---------------------------------------+
            |          n = 32                       |
            |          summation(n, 1) = 528        |
            |          formula result = 528         |
            |             +------+                  |
            |             |  OK  |                  |
            |             +------+                  |
            +---------------------------------------+
    """
    print_exercise_name()

    # TODO 2b: Write code to use the summation function
    n = (int(easygui.integerbox("What is your range?")))
    exp = int(easygui.integerbox("What is your exponent?"))
    sum = summation(n,exp)
    formula = n*(n+1)//2
    easygui.msgbox("n = {:d}\nsummation (n,exp) = {:d}\nformula result = {:d}".format(n, sum, formula))


# TODO 2a: In the space below, write the summation function

def summation(n,exp):
    sum = 0
    for i in range(1,n+1):
        sum = sum + (i**exp)
    return sum

def exercise3
    You will need to retrieve the Lab08_exercise_3.pdf file for detailed instructions
    and detailed screenshots of the images you will be producing in this exercise.
    """
    print_exercise_name()

    # Create the turtle screen and two turtles (leave this as the first line).
    screen, artist, writer = turtle_setup()

    # TODO 3b: In the space below, write code to use the draw_inner_square function
    #draw_inner_square(artist,0.75,300)
    #draw_square(artist,640)

    # TODO 3d: In the space below, write code to use the draw_inner_squares function
    #():
    """draw_inner_squares(artist, 300, 14)

    # TODO 3f: In the space below, write code to use the draw_art function
    draw_art(artist,300)  # Remove the pass statement (and this comment) when writing your own code.

    # Wait for the user to click before closing the window (leave this as the last line).
    screen.exitonclick()


# TODO 3a: In the space below, write the draw_inner_square function
def draw_inner_square(turtle, percent,outer_size):

    a = percent*outer_size
    b = outer_size - a
    inner_size = math.sqrt(a**2+b**2)
    alpha = degrees(atan2(a, b))
    turtle.left(90)
    turtle.penup()
    turtle.forward(a)
    turtle.right(alpha+90)
    turtle.pendown()
    draw_square(turtle,inner_size)
    turtle.penup()
    turtle.left(alpha+90)
    turtle.backward(a)
    turtle.right(90)
    turtle.pendown()


# TODO 3c: In the space below, write the draw_inner_squares function
def draw_inner_squares(turtle, size, amount):
    percentage = 1/(amount+1)
    for i in range(1,amount+1):
        percent = 1 - percentage*i
        a = percent * size
        b = size - a
        inner_size = sqrt((a ** 2) + (b ** 2))
        alpha = degrees(atan2(a, b))
        draw_inner_square(turtle,percent,size)

# TODO 3e: In the space below, write the draw_art function
def draw_art(turtle, size):
    for i in range(4):
        draw_square(turtle,300)
        draw_inner_squares(turtle,300,14)
        turtle.left(90)

# Leave this function below those written in steps 3a, 3c, and 3e.
def draw_square(art, size):
    """Use the given turtle to draw a square with one corner at the turtle's current location.

    :param turtle.Turtle art: The turtle to do the drawing.
    :param int size: The length of one side of the square.
    """

    for _ in range(4):
        art.forward(size)
        art.left(90)


"""
Challenge Exercises:

1.	Use the functions written in the previous exercise to
    create different geometric patterns.
2.	Write new functions similar to those in the previous exercise
    to create different geometric patterns.
"""


# ======================================================================
# DO NOT EDIT BELOW THIS LINE
# ======================================================================

def turtle_setup():
    """Setup the turtle environment with a screen and two turtles, one for drawing and one for writing.

    Using separate turtles for drawing and writing makes it easy to clear one or the other by
    doing artist.clear() or writer.clear() to clear only the drawing or writing, respectively.

    :return: The screen, a drawing turtle, and a writing turtle.
    :rtype: (turtle.Screen, turtle.Turtle, turtle.Turtle)
    """
    # Create the turtle graphics screen and set a few basic properties.
    screen = turtle.Screen()
    screen.setup(WIDTH, HEIGHT, MARGIN, MARGIN)
    screen.bgcolor("SkyBlue")

    # Create two turtles, one for drawing and one for writing.
    turtle.TurtleScreen._RUNNING = True  # Get around bug in v3.5.2 http://bugs.python.org/issue26571
    artist = turtle.Turtle()
    writer = turtle.Turtle()

    # Change the artist turtle's shape so the artist and writer are distinguishable.
    artist.shape("turtle")

    # Make the animation as fast as possible and hide the turtles.
    if DRAW_FAST:
        screen.delay(0)
        artist.hideturtle()
        artist.speed("fastest")
        writer.hideturtle()
        writer.speed("fastest")

    # Set a few properties of the writing turtle useful since it will only be writing.
    writer.setheading(90)  # Straight up, which makes it look sort of like a cursor.
    writer.penup()  # A turtle's pen does not have to be down to write text.
    writer.setposition(0, HEIGHT // 2 - FONT_SIZE * 2)  # Centered at top of the screen.

    return screen, artist, writer


def print_exercise_name():
    """Print the name and docstring of the calling function (i.e., the current exercise.)"""
    try:
        import inspect
        name = inspect.getframeinfo(inspect.currentframe().f_back).function
        doc = inspect.getdoc(globals()[name])
        print('\n\033[94m{}\n{}\n\033[92m{}\033[99m'.format(name, "=" * len(name), doc), flush=True)
    except AttributeError:
        pass  # Likely caused by lack of stack frame support where currentframe() returns None.
    except KeyError:
        pass  # In case the function name is not found in the globals dictionary.


# The following two lines are always the last lines in a source file and they
# start the execution of the program; everything above was just definitions.
if __name__ == "__main__":
    main()
